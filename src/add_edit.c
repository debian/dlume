
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "gui.h"
#include "add_edit.h"
#include "data.h"
#include "prefs.h"
#include "i18n.h"

/* edit record window */
static GtkWidget 	*edit_window;
static GtkWidget	*e_entry[nfields];
static GtkTextBuffer	*notes_textinfo;
static GtkWidget	*close_button;
static GtkWidget	*ok_button;

/* new contact window */
static GtkWidget	*new_contact_window;
static GtkTextBuffer	*notes_textinfo;
static gchar		tmpbuf[TMPBUF_SIZE];
static GtkTextIter	p_iter;

static recordstr rec_entry;

static gpointer fields_ptr[]= {
	rec_entry.first_name, rec_entry.last_name, rec_entry.nickname, rec_entry.birthday,
	rec_entry.address, rec_entry.post_code,
	rec_entry.city,	rec_entry.state, rec_entry.country,
	rec_entry.home_phone_1, rec_entry.home_phone_2,
	rec_entry.home_phone_3, rec_entry.home_phone_4,
	rec_entry.work_phone_1, rec_entry.work_phone_2, rec_entry.work_phone_3,
	rec_entry.work_phone_4, rec_entry.cell_phone_1,
	rec_entry.cell_phone_2, rec_entry.cell_phone_3, rec_entry.cell_phone_4,
	rec_entry.email_1, rec_entry.email_2, rec_entry.email_3,
	rec_entry.email_4, rec_entry.url_1, rec_entry.url_2,
	rec_entry.url_3, rec_entry.url_4, rec_entry.notes
};


/*------------------------------------------------------------------------------*/

int gui_add_rkey (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
gint l;

	l = strlen((gchar *)gtk_entry_get_text(GTK_ENTRY(e_entry[FIRST_NAME])));
	l += strlen((gchar *)gtk_entry_get_text(GTK_ENTRY(e_entry[LAST_NAME])));

	if(l)
		gtk_widget_set_sensitive (ok_button, TRUE);
	else
		gtk_widget_set_sensitive (ok_button, FALSE);

	return FALSE;
}

/*------------------------------------------------------------------------------*/
/* add record callback */

void gui_new_contact_action( GtkWidget *widget, gpointer data )
{
gint i;
GtkTextIter a_iter, b_iter;

	memset(&rec_entry, 0, sizeof(recordstr));

	for(i=0;i<nfields-1;i++) {
		strncpy(fields_ptr[i], gtk_entry_get_text(GTK_ENTRY(e_entry[i])),
				fields_size[i]);
		strcat(fields_ptr[i], "\0");
	}	

	gtk_text_buffer_get_bounds (notes_textinfo, &a_iter, &b_iter);

	strncpy(fields_ptr[NOTES],
			gtk_text_buffer_get_text(notes_textinfo, &a_iter, &b_iter, FALSE),
			size_notes);


	if(strlen(rec_entry.first_name) || strlen(rec_entry.last_name)) {

		data_add_record(&rec_entry);

		gui_display_records_list();
		gui_update_records_list((gchar *)gtk_entry_get_text(GTK_ENTRY(find_entry)));

		gui_show_status(_("Contact added sucessfully."));

	} else {

		gui_show_status(_("Contact needs first or last name!"));

	}

	gdk_window_get_root_origin (new_contact_window->window,
								&config.contact_window_x, &config.contact_window_y);

	gtk_widget_destroy (new_contact_window);
}

/*------------------------------------------------------------------------------*/
/* close window callback */

void gui_close_new_contact_window( GtkWidget *widget, gpointer data )
{
	if(config.restore_windows_pos)
		gdk_window_get_root_origin (new_contact_window->window,
									&config.contact_window_x, &config.contact_window_y);

	gtk_widget_destroy (new_contact_window);
	gui_show_status(_("Idle"));

}

/*------------------------------------------------------------------------------*/

void gui_create_new_contact_window(void)
{
GtkWidget *vbox1, *vbox3, *vbox4, *vbox5, *vbox6, *vbox7, *vbox8, *vbox9;
GtkWidget *hbox7, *hbox23, *hbox24, *hbox8, *hbox9, *hbox10, *hbox11;
GtkWidget *hbox12, *hbox13, *hbox14, *hbox15, *hbox16, *hbox17, *hbox18;
GtkWidget *hbox19, *hbox20, *hbox21, *hbox22, *hbox25;
GtkWidget *frame;
GtkWidget *label;
GtkWidget *hseparator;
GtkWidget *cancel_button;
GtkWidget *hbuttonbox;
GtkWidget *scrolledwindow;
GdkPixmap *icon;
GdkBitmap *icon_mask;
GtkStyle  *style;
GtkSizeGroup *sg_label, *sg_entry;

	gui_show_status(_("Enter the data..."));

	sg_label = gtk_size_group_new(GTK_SIZE_GROUP_BOTH);
	sg_entry = gtk_size_group_new(GTK_SIZE_GROUP_BOTH);

	new_contact_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for(GTK_WINDOW(new_contact_window),GTK_WINDOW(main_window));
	gtk_container_set_border_width (GTK_CONTAINER (new_contact_window), 6);
	gtk_window_set_modal(GTK_WINDOW(new_contact_window), TRUE);
	gtk_window_set_title (GTK_WINDOW (new_contact_window), _("Add new contact..."));

	gtk_window_set_resizable (GTK_WINDOW (new_contact_window), TRUE);
	gtk_widget_set_size_request (GTK_WIDGET(new_contact_window), add_width, add_height);

	if(config.restore_windows_pos)
		gtk_window_move (GTK_WINDOW (new_contact_window),
									config.contact_window_x, config.contact_window_y);
	
	g_signal_connect (G_OBJECT (new_contact_window), "delete_event",
						G_CALLBACK(gui_close_new_contact_window), NULL);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (new_contact_window), vbox1);

	scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (vbox1), scrolledwindow);
	gtk_widget_show (scrolledwindow);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	vbox4 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox4);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW (scrolledwindow), vbox4);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Personal</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame), vbox3);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	hbox7 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox7);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox7, FALSE, TRUE, 2);

	label = gtk_label_new (_("First Name: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox7), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[FIRST_NAME] = gtk_entry_new();
	g_signal_connect (G_OBJECT(e_entry[FIRST_NAME]), "key_release_event",
						G_CALLBACK(gui_add_rkey), NULL);
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[FIRST_NAME]), size_name-1);
	gtk_widget_show (e_entry[FIRST_NAME]);
	gtk_box_pack_start (GTK_BOX (hbox7), e_entry[FIRST_NAME], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[FIRST_NAME]);

	hbox23 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox23);
	gtk_box_pack_start (GTK_BOX (hbox7), hbox23, TRUE, TRUE, 2);

	label = gtk_label_new (_("Last Name: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox7), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[LAST_NAME] = gtk_entry_new();
	g_signal_connect (G_OBJECT(e_entry[LAST_NAME]), "key_release_event",
						G_CALLBACK(gui_add_rkey), NULL);
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[LAST_NAME]), size_name-1);
	gtk_widget_show (e_entry[LAST_NAME]);
	gtk_box_pack_end (GTK_BOX (hbox7), e_entry[LAST_NAME], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[LAST_NAME]);

	hbox24 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox24);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox24, TRUE, TRUE, 2);

	label = gtk_label_new (_("Nickname: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox24), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[NICKNAME] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[NICKNAME]), size_name-1);
	gtk_widget_show (e_entry[NICKNAME]);
	gtk_box_pack_start (GTK_BOX (hbox24), e_entry[NICKNAME], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[NICKNAME]);

	hbox23 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox23);
	gtk_box_pack_start (GTK_BOX (hbox24), hbox23, TRUE, TRUE, 2);

	label = gtk_label_new (_("Birthday: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox24), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[BIRTHDAY] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[BIRTHDAY]), size_birthday-1);
	gtk_widget_show (e_entry[BIRTHDAY]);
	gtk_box_pack_start (GTK_BOX (hbox24), e_entry[BIRTHDAY], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[BIRTHDAY]);

	hbox8 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox8);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox8, TRUE, TRUE, 2);

	label = gtk_label_new (_("Address: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox8), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[ADDRESS] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[ADDRESS]), size_address-1);
	gtk_widget_show (e_entry[ADDRESS]);
	gtk_box_pack_start (GTK_BOX (hbox8), e_entry[ADDRESS], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[ADDRESS]);

	hbox23 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox23);
	gtk_box_pack_start (GTK_BOX (hbox8), hbox23, TRUE, TRUE, 2);

	label = gtk_label_new (_("Post Code:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox8), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[POST_CODE] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[POST_CODE]), size_post_code-1);
	gtk_widget_show (e_entry[POST_CODE]);
	gtk_box_pack_start (GTK_BOX (hbox8), e_entry[POST_CODE], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[POST_CODE]);

	hbox9 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox9);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox9, TRUE, TRUE, 2);

	label = gtk_label_new (_("City: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox9), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[CITY] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[CITY]), size_city-1);
	gtk_widget_show (e_entry[CITY]);
	gtk_box_pack_start (GTK_BOX (hbox9), e_entry[CITY], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[CITY]);

	hbox23 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox23);
	gtk_box_pack_start (GTK_BOX (hbox9), hbox23, TRUE, TRUE, 2);

	label = gtk_label_new (_("State:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox9), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[STATE] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[STATE]), size_state-1);
	gtk_widget_show (e_entry[STATE]);
	gtk_box_pack_start (GTK_BOX (hbox9), e_entry[STATE], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[STATE]);

	hbox25 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox25);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox25, TRUE, TRUE, 2);

	label = gtk_label_new (_("Country:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox25), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[COUNTRY] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[COUNTRY]), size_country-1);
	gtk_widget_show (e_entry[COUNTRY]);
	gtk_box_pack_start (GTK_BOX (hbox25), e_entry[COUNTRY], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[COUNTRY]);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, FALSE, TRUE, 1);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Home phones</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox9 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox9);
	gtk_container_add (GTK_CONTAINER (frame), vbox9);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox9), hseparator, FALSE, TRUE, 4);

	hbox11 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox11);
	gtk_box_pack_start (GTK_BOX (vbox9), hbox11, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 1:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox11), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[HOME_PHONE_1] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_1]), size_phone-1);
	gtk_widget_show (e_entry[HOME_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (hbox11), e_entry[HOME_PHONE_1], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[HOME_PHONE_1]);

	hbox10 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox10);
	gtk_box_pack_start (GTK_BOX (hbox11), hbox10, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 2:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox11), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[HOME_PHONE_2] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_2]), size_phone-1);
	gtk_widget_show (e_entry[HOME_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (hbox11), e_entry[HOME_PHONE_2], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[HOME_PHONE_2]);

	hbox12 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox12);
	gtk_box_pack_start (GTK_BOX (vbox9), hbox12, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 3:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox12), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[HOME_PHONE_3] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_3]), size_phone-1);
	gtk_widget_show (e_entry[HOME_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (hbox12), e_entry[HOME_PHONE_3], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[HOME_PHONE_3]);

	hbox10 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox10);
	gtk_box_pack_start (GTK_BOX (hbox12), hbox10, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 4:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox12), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[HOME_PHONE_4] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_4]), size_phone-1);
	gtk_widget_show (e_entry[HOME_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (hbox12), e_entry[HOME_PHONE_4], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[HOME_PHONE_4]);

	/*-----------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Work phones</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox5 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox5);
	gtk_container_add (GTK_CONTAINER (frame), vbox5);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox5), hseparator, FALSE, TRUE, 4);

	hbox14 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox14);
	gtk_box_pack_start (GTK_BOX (vbox5), hbox14, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 1:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox14), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[WORK_PHONE_1] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_1]), size_phone-1);
	gtk_widget_show (e_entry[WORK_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (hbox14), e_entry[WORK_PHONE_1], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[WORK_PHONE_1]);

	hbox13 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox13);
	gtk_box_pack_start (GTK_BOX (hbox14), hbox13, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 2:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox14), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[WORK_PHONE_2] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_2]), size_phone-1);
	gtk_widget_show (e_entry[WORK_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (hbox14), e_entry[WORK_PHONE_2], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[WORK_PHONE_2]);

	hbox15 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox15);
	gtk_box_pack_start (GTK_BOX (vbox5), hbox15, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 3:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox15), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[WORK_PHONE_3] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_3]), size_phone-1);
	gtk_widget_show (e_entry[WORK_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (hbox15), e_entry[WORK_PHONE_3], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[WORK_PHONE_3]);

	hbox13 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox13);
	gtk_box_pack_start (GTK_BOX (hbox15), hbox13, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 4:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox15), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[WORK_PHONE_4] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_4]), size_phone-1);
	gtk_widget_show (e_entry[WORK_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (hbox15), e_entry[WORK_PHONE_4], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[WORK_PHONE_4]);

	/*-----------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Cell phones</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox6 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox6);
	gtk_container_add (GTK_CONTAINER (frame), vbox6);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox6), hseparator, FALSE, TRUE, 4);

	hbox17 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox17);
	gtk_box_pack_start (GTK_BOX (vbox6), hbox17, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 1:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox17), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[CELL_PHONE_1] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_1]), size_phone-1);
	gtk_widget_show (e_entry[CELL_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (hbox17), e_entry[CELL_PHONE_1], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[CELL_PHONE_1]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox17), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 2:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox17), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[CELL_PHONE_2] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_2]), size_phone-1);
	gtk_widget_show (e_entry[CELL_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (hbox17), e_entry[CELL_PHONE_2], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[CELL_PHONE_2]);

	hbox18 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox18);
	gtk_box_pack_start (GTK_BOX (vbox6), hbox18, FALSE, TRUE, 2);

	label = gtk_label_new (_("Phone 3:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox18), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[CELL_PHONE_3] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_3]), size_phone-1);
	gtk_widget_show (e_entry[CELL_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (hbox18), e_entry[CELL_PHONE_3], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[CELL_PHONE_3]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox18), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("Phone 4:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox18), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[CELL_PHONE_4] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_4]), size_phone-1);
	gtk_widget_show (e_entry[CELL_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (hbox18), e_entry[CELL_PHONE_4], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[CELL_PHONE_4]);

	/*-----------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>E-mails </b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox7 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox7);
	gtk_container_add (GTK_CONTAINER (frame), vbox7);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox7), hseparator, FALSE, TRUE, 4);

	hbox19 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox19);
	gtk_box_pack_start (GTK_BOX (vbox7), hbox19, FALSE, TRUE, 2);

	label = gtk_label_new (_("E-mail 1: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox19), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[EMAIL_1] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_1]), size_email-1);
	gtk_widget_show (e_entry[EMAIL_1]);
	gtk_box_pack_start (GTK_BOX (hbox19), e_entry[EMAIL_1], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[EMAIL_1]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox19), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("E-mail 2: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox19), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[EMAIL_2] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_2]), size_email-1);
	gtk_widget_show (e_entry[EMAIL_2]);
	gtk_box_pack_start (GTK_BOX (hbox19), e_entry[EMAIL_2], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[EMAIL_2]);

	hbox20 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox20);
	gtk_box_pack_start (GTK_BOX (vbox7), hbox20, FALSE, TRUE, 2);

	label = gtk_label_new (_("E-mail 3: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox20), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[EMAIL_3] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_3]), size_email-1);
	gtk_widget_show (e_entry[EMAIL_3]);
	gtk_box_pack_start (GTK_BOX (hbox20), e_entry[EMAIL_3], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[EMAIL_3]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox20), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("E-mail 4: "));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox20), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[EMAIL_4] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_4]), size_email-1);
	gtk_widget_show (e_entry[EMAIL_4]);
	gtk_box_pack_start (GTK_BOX (hbox20), e_entry[EMAIL_4], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[EMAIL_4]);

	/*-----------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>WWW pages </b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox8 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox8);
	gtk_container_add (GTK_CONTAINER (frame), vbox8);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox8), hseparator, FALSE, TRUE, 4);

	hbox21 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox21);
	gtk_box_pack_start (GTK_BOX (vbox8), hbox21, FALSE, TRUE, 2);

	label = gtk_label_new (_("Website 1:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox21), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[URL_1] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_1]), size_url-1);
	gtk_widget_show (e_entry[URL_1]);
	gtk_box_pack_start (GTK_BOX (hbox21), e_entry[URL_1], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[URL_1]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox21), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("Website 2:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox21), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[URL_2] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_2]), size_url-1);
	gtk_widget_show (e_entry[URL_2]);
	gtk_box_pack_start (GTK_BOX (hbox21), e_entry[URL_2], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[URL_2]);

	hbox22 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox22);
	gtk_box_pack_start (GTK_BOX (vbox8), hbox22, FALSE, TRUE, 2);

	label = gtk_label_new (_("Website 3:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox22), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[URL_3] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_3]), size_url-1);
	gtk_widget_show (e_entry[URL_3]);
	gtk_box_pack_start (GTK_BOX (hbox22), e_entry[URL_3], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[URL_3]);

	hbox16 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox16);
	gtk_box_pack_start (GTK_BOX (hbox22), hbox16, TRUE, TRUE, 2);

	label = gtk_label_new (_("Website 4:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox22), label, FALSE, FALSE, 2);
	gtk_size_group_add_widget(sg_label, label);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

	e_entry[URL_4] = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_4]), size_url-1);
	gtk_widget_show (e_entry[URL_4]);
	gtk_box_pack_start (GTK_BOX (hbox22), e_entry[URL_4], FALSE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, e_entry[URL_4]);

	/*-----------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Additional info </b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	vbox8 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox8);
	gtk_container_add (GTK_CONTAINER (frame), vbox8);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox8), hseparator, FALSE, TRUE, 4);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox8), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);

	scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow);
	gtk_container_add (GTK_CONTAINER (frame), scrolledwindow);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	notes_textinfo = gtk_text_buffer_new (NULL);
	gtk_text_buffer_get_iter_at_offset (notes_textinfo, &p_iter, 0);
	e_entry[NOTES] =  gtk_text_view_new_with_buffer (notes_textinfo);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (e_entry[NOTES]), GTK_WRAP_WORD);
	gtk_widget_set_size_request (e_entry[NOTES], -1, 150);
	gtk_widget_show (e_entry[NOTES]);
	gtk_container_add (GTK_CONTAINER (scrolledwindow),e_entry[NOTES]);
	gtk_container_set_border_width (GTK_CONTAINER (e_entry[NOTES]), 2);

	/*-----------------------------------------------------------------*/

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

	hbuttonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbuttonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox, FALSE, FALSE, 4);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (hbuttonbox), 16);

	cancel_button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
	gtk_widget_show (cancel_button);
	g_signal_connect (G_OBJECT (cancel_button), "clicked",
						G_CALLBACK (gui_close_new_contact_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), cancel_button);
	GTK_WIDGET_SET_FLAGS (cancel_button, GTK_CAN_DEFAULT);

	ok_button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_widget_show (ok_button);
	g_signal_connect (G_OBJECT (ok_button), "clicked",
						G_CALLBACK (gui_new_contact_action), NULL);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), ok_button);
	GTK_WIDGET_SET_FLAGS (ok_button, GTK_CAN_DEFAULT);

	gtk_widget_set_sensitive (ok_button, FALSE);

	gtk_widget_show (new_contact_window);

	style = gtk_widget_get_style(GTK_WIDGET(new_contact_window));
	icon = gdk_pixmap_create_from_xpm_d (new_contact_window->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
	gdk_window_set_icon (new_contact_window->window, NULL, icon, icon_mask);

	gtk_widget_grab_focus (e_entry[FIRST_NAME]);

}

/*------------------------------------------------------------------------------*/

int gui_edit_rkey (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
gint l;

	l = strlen((gchar *)gtk_entry_get_text(GTK_ENTRY(e_entry[FIRST_NAME])));
	l += strlen((gchar *)gtk_entry_get_text(GTK_ENTRY(e_entry[LAST_NAME])));

	if(l)
		gtk_widget_set_sensitive (close_button, TRUE);
	else
		gtk_widget_set_sensitive (close_button, FALSE);

	return FALSE;
}

/*------------------------------------------------------------------------------*/

void gui_close_cancel_edit_window( GtkWidget *widget, gpointer data )
{
	gdk_window_get_root_origin (edit_window->window,
							&config.edit_window_x, &config.edit_window_y);

	gtk_widget_destroy (edit_window);

	gui_show_status(_("Idle"));

}

/*------------------------------------------------------------------------------*/
/* close window callback */

void gui_close_edit_window( GtkWidget *widget, gpointer data )
{
gint i, k, r;
recordstr *rdata;
GtkTextIter a_iter, b_iter;

	memset(&rec_entry, 0, sizeof(recordstr));

	strcpy(tmpbuf, gtk_entry_get_text(GTK_ENTRY(e_entry[FIRST_NAME])));
	tmpbuf[fields_size[FIRST_NAME]] = '\0';
	strcpy(fields_ptr[FIRST_NAME], tmpbuf);

	strcpy(tmpbuf, gtk_entry_get_text(GTK_ENTRY(e_entry[LAST_NAME])));
	tmpbuf[fields_size[LAST_NAME]] = '\0';
	strcpy(fields_ptr[LAST_NAME], tmpbuf);


	if(strlen(fields_ptr[FIRST_NAME]) || strlen(fields_ptr[LAST_NAME])) {

	k = find_record(current_fn, current_ln);
	r =	gui_list_get_row ();

	if (k!=-1) {


		for(i=2;i<nfields-1;i++) {
	            strcpy(tmpbuf, gtk_entry_get_text(GTK_ENTRY(e_entry[i])));
		    tmpbuf[fields_size[i]] = '\0';
		    strcpy(fields_ptr[i], tmpbuf);
		}

		gtk_text_buffer_get_bounds (notes_textinfo, &a_iter, &b_iter);

		strncpy(fields_ptr[NOTES],
				gtk_text_buffer_get_text(notes_textinfo, &a_iter, &b_iter, FALSE),
				size_notes);

		rdata = g_slist_nth_data (list, k);
		memcpy(rdata, &rec_entry, sizeof(recordstr));

		strcpy(current_fn, fields_ptr[FIRST_NAME]);
		strcpy(current_ln, fields_ptr[LAST_NAME]);

		gui_display_entry (current_fn, current_ln);
		gui_display_records_list();
		gui_update_records_list((gchar *)gtk_entry_get_text(GTK_ENTRY(find_entry)));

		gui_list_set_row(r);
	}

	if(config.restore_windows_pos)
		gdk_window_get_root_origin (edit_window->window,
								&config.edit_window_x, &config.edit_window_y);

	gtk_widget_destroy (edit_window);
	gui_show_status(_("Idle"));

	} else {

		gui_show_status(_("First or last name must be filled!"));

	}
}

/*------------------------------------------------------------------------------*/

void gui_edit_contact_window(void)
{
GtkWidget *vbox1, *vbox2, *vbox3, *vbox4;
GtkWidget *hbox1, *hbox2, *hbox3, *hbox3a, *hbox4, *hbox5, *hbox6, *hbox7;
GtkWidget *hbox7a, *hbox8, *hbox9, *hbox10, *hbox11, *hbox12, *hbox13;
GtkWidget *hbox14, *hbox15, *hbox16, *hbox17, *hbox18, *hbox19, *hbox20;
GtkWidget *hbox21, *hbox22, *hbox23, *hbox24, *hbox25, *hbox26, *hbox27;
GtkWidget *label;
GtkWidget *frame;
GtkWidget *scrolledwindow;
GtkWidget *scrolledwindow2;
GtkWidget *hbuttonbox;
GtkWidget *cancel_button;
GtkWidget *hseparator;
GdkPixmap *icon;
GdkBitmap *icon_mask;
GtkStyle  *style;
gint      i, k;
recordstr *rdata;
GtkSizeGroup *sg_label;


	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		gui_show_status(_("Modify the data..."));

		sg_label = gtk_size_group_new(GTK_SIZE_GROUP_BOTH);

		edit_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_transient_for(GTK_WINDOW(edit_window),GTK_WINDOW(main_window));
		gtk_window_set_title (GTK_WINDOW (edit_window), _("Edit contact..."));
		gtk_container_set_border_width (GTK_CONTAINER (edit_window), 6);
		gtk_window_set_modal(GTK_WINDOW(edit_window), TRUE);

		gtk_window_set_resizable (GTK_WINDOW (main_window), TRUE);
		gtk_widget_set_size_request (GTK_WIDGET(edit_window), edit_width, edit_height);

		if(config.restore_windows_pos)
			gtk_window_move (GTK_WINDOW (edit_window),
								config.edit_window_x, config.edit_window_y);

		g_signal_connect (G_OBJECT (edit_window), "delete_event",
							G_CALLBACK(gui_close_edit_window), NULL);

		vbox1 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox1);
		gtk_container_add (GTK_CONTAINER (edit_window), vbox1);


		vbox2 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox2);
		gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

		scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_show (scrolledwindow);
		gtk_box_pack_start (GTK_BOX (vbox2), scrolledwindow, TRUE, TRUE, 0);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

		vbox4 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox4);
		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW (scrolledwindow), vbox4);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>Personal</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox1 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox1);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox1, TRUE, TRUE, 0);

		label = gtk_label_new (_("First Name: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox1), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[FIRST_NAME] = gtk_entry_new();
		g_signal_connect (G_OBJECT(e_entry[FIRST_NAME]), "key_release_event",
							G_CALLBACK(gui_edit_rkey), NULL);
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[FIRST_NAME]), size_name-1);
		gtk_widget_show (e_entry[FIRST_NAME]);
		gtk_box_pack_start (GTK_BOX (hbox1), e_entry[FIRST_NAME], TRUE, TRUE, 2);

		hbox2 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox2);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox2, TRUE, TRUE, 0);

		label = gtk_label_new (_("Last Name: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[LAST_NAME] = gtk_entry_new();
		g_signal_connect (G_OBJECT(e_entry[LAST_NAME]), "key_release_event",
							G_CALLBACK(gui_edit_rkey), NULL);
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[LAST_NAME]), size_name-1);
		gtk_widget_show (e_entry[LAST_NAME]);
		gtk_box_pack_start (GTK_BOX (hbox2), e_entry[LAST_NAME], TRUE, TRUE, 2);

		hbox3a = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox3a);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox3a, TRUE, TRUE, 0);

		label = gtk_label_new (_("Nickname: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox3a), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[NICKNAME] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[NICKNAME]), size_name-1);
		gtk_widget_show (e_entry[NICKNAME]);
		gtk_box_pack_start (GTK_BOX (hbox3a), e_entry[NICKNAME], TRUE, TRUE, 2);

		hbox3 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox3);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox3, TRUE, TRUE, 0);

		label = gtk_label_new (_("Birthday: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox3), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[BIRTHDAY] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[BIRTHDAY]), size_birthday-1);
		gtk_widget_show (e_entry[BIRTHDAY]);
		gtk_box_pack_start (GTK_BOX (hbox3), e_entry[BIRTHDAY], TRUE, TRUE, 2);

		hbox4 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox4);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox4, TRUE, TRUE, 0);

		label = gtk_label_new (_("Address: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox4), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[ADDRESS] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[ADDRESS]), size_address-1);
		gtk_widget_show (e_entry[ADDRESS]);
		gtk_box_pack_start (GTK_BOX (hbox4), e_entry[ADDRESS], TRUE, TRUE, 2);

		hbox5 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox5);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox5, TRUE, TRUE, 0);

		label = gtk_label_new (_("Post Code: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox5), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[POST_CODE] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[POST_CODE]), size_post_code-1);
		gtk_widget_show (e_entry[POST_CODE]);
		gtk_box_pack_start (GTK_BOX (hbox5), e_entry[POST_CODE], TRUE, TRUE, 2);

		hbox6 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox6);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox6, TRUE, TRUE, 0);

		label = gtk_label_new (_("City: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox6), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[CITY] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[CITY]), size_city-1);
		gtk_widget_show (e_entry[CITY]);
		gtk_box_pack_start (GTK_BOX (hbox6), e_entry[CITY], TRUE, TRUE, 2);

		hbox7a = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox7a);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox7a, TRUE, TRUE, 0);

		label = gtk_label_new (_("State: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox7a), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[STATE] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[STATE]), size_state-1);
		gtk_widget_show (e_entry[STATE]);
		gtk_box_pack_start (GTK_BOX (hbox7a), e_entry[STATE], TRUE, TRUE, 2);

		hbox7 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox7);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox7, TRUE, TRUE, 0);

		label = gtk_label_new (_("Country: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox7), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[COUNTRY] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[COUNTRY]), size_country-1);
		gtk_widget_show (e_entry[COUNTRY]);
		gtk_box_pack_start (GTK_BOX (hbox7), e_entry[COUNTRY], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>Home phones</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox8 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox8);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox8, TRUE, TRUE, 0);

		label = gtk_label_new (_("Home phone 1: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox8), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[HOME_PHONE_1] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_1]), size_phone-1);
		gtk_widget_show (e_entry[HOME_PHONE_1]);
		gtk_box_pack_start (GTK_BOX (hbox8), e_entry[HOME_PHONE_1], TRUE, TRUE, 2);

		hbox9 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox9);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox9, TRUE, TRUE, 0);

		label = gtk_label_new (_("Home phone 2: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox9), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[HOME_PHONE_2] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_2]), size_phone-1);
		gtk_widget_show (e_entry[HOME_PHONE_2]);
		gtk_box_pack_start (GTK_BOX (hbox9), e_entry[HOME_PHONE_2], TRUE, TRUE, 2);

		hbox10 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox10);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox10, TRUE, TRUE, 0);

		label = gtk_label_new (_("Home phone 3: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox10), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[HOME_PHONE_3] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_3]), size_phone-1);
		gtk_widget_show (e_entry[HOME_PHONE_3]);
		gtk_box_pack_start (GTK_BOX (hbox10), e_entry[HOME_PHONE_3], TRUE, TRUE, 2);

		hbox11 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox11);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox11, TRUE, TRUE, 0);

		label = gtk_label_new (_("Home phone 4: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox11), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[HOME_PHONE_4] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[HOME_PHONE_4]), size_phone-1);
		gtk_widget_show (e_entry[HOME_PHONE_4]);
		gtk_box_pack_start (GTK_BOX (hbox11), e_entry[HOME_PHONE_4], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>Work phones</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox12 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox12);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox12, TRUE, TRUE, 0);

		label = gtk_label_new (_("Work phone 1: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox12), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[WORK_PHONE_1] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_1]), size_phone-1);
		gtk_widget_show (e_entry[WORK_PHONE_1]);
		gtk_box_pack_start (GTK_BOX (hbox12), e_entry[WORK_PHONE_1], TRUE, TRUE, 2);

		hbox13 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox13);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox13, TRUE, TRUE, 0);

		label = gtk_label_new (_("Work phone 2: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox13), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[WORK_PHONE_2] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_2]), size_phone-1);
		gtk_widget_show (e_entry[WORK_PHONE_2]);
		gtk_box_pack_start (GTK_BOX (hbox13), e_entry[WORK_PHONE_2], TRUE, TRUE, 2);

		hbox14 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox14);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox14, TRUE, TRUE, 0);

		label = gtk_label_new (_("Work phone 3: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox14), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[WORK_PHONE_3] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_3]), size_phone-1);
		gtk_widget_show (e_entry[WORK_PHONE_3]);
		gtk_box_pack_start (GTK_BOX (hbox14), e_entry[WORK_PHONE_3], TRUE, TRUE, 2);

		hbox15 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox15);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox15, TRUE, TRUE, 0);

		label = gtk_label_new (_("Work phone 4: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox15), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[WORK_PHONE_4] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[WORK_PHONE_4]), size_phone-1);
		gtk_widget_show (e_entry[WORK_PHONE_4]);
		gtk_box_pack_start (GTK_BOX (hbox15), e_entry[WORK_PHONE_4], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>Cell phones</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox16 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox16);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox16, TRUE, TRUE, 0);

		label = gtk_label_new (_("Cell phone 1: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox16), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[CELL_PHONE_1] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_1]), size_phone-1);
		gtk_widget_show (e_entry[CELL_PHONE_1]);
		gtk_box_pack_start (GTK_BOX (hbox16), e_entry[CELL_PHONE_1], TRUE, TRUE, 2);

		hbox17 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox17);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox17, TRUE, TRUE, 0);

		label = gtk_label_new (_("Cell phone 2: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox17), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[CELL_PHONE_2] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_2]), size_phone-1);
		gtk_widget_show (e_entry[CELL_PHONE_2]);
		gtk_box_pack_start (GTK_BOX (hbox17), e_entry[CELL_PHONE_2], TRUE, TRUE, 2);

		hbox18 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox18);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox18, TRUE, TRUE, 0);

		label = gtk_label_new (_("Cell phone 3: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox18), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[CELL_PHONE_3] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_3]), size_phone-1);
		gtk_widget_show (e_entry[CELL_PHONE_3]);
		gtk_box_pack_start (GTK_BOX (hbox18), e_entry[CELL_PHONE_3], TRUE, TRUE, 2);

		hbox19 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox19);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox19, TRUE, TRUE, 0);

		label = gtk_label_new (_("Cell phone 4: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox19), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[CELL_PHONE_4] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[CELL_PHONE_4]), size_phone-1);
		gtk_widget_show (e_entry[CELL_PHONE_4]);
		gtk_box_pack_start (GTK_BOX (hbox19), e_entry[CELL_PHONE_4], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>E-mails</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox20 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox20);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox20, TRUE, TRUE, 0);

		label = gtk_label_new (_("E-mail 1: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox20), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[EMAIL_1] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_1]), size_email-1);
		gtk_widget_show (e_entry[EMAIL_1]);
		gtk_box_pack_start (GTK_BOX (hbox20), e_entry[EMAIL_1], TRUE, TRUE, 2);

		hbox21 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox21);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox21, TRUE, TRUE, 0);

		label = gtk_label_new (_("E-mail 2: "));
		gtk_widget_show (label);
		gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox21), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[EMAIL_2] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_2]), size_email-1);
		gtk_widget_show (e_entry[EMAIL_2]);
		gtk_box_pack_start (GTK_BOX (hbox21), e_entry[EMAIL_2], TRUE, TRUE, 2);

		hbox22 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox22);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox22, TRUE, TRUE, 0);

		label = gtk_label_new (_("E-mail 3: "));
		gtk_widget_show (label);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox22), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[EMAIL_3] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_3]), size_email-1);
		gtk_widget_show (e_entry[EMAIL_3]);
		gtk_box_pack_start (GTK_BOX (hbox22), e_entry[EMAIL_3], TRUE, TRUE, 2);

		hbox23 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox23);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox23, TRUE, TRUE, 0);

		label = gtk_label_new (_("E-mail 4: "));
		gtk_widget_show (label);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox23), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[EMAIL_4] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[EMAIL_4]), size_email-1);
		gtk_widget_show (e_entry[EMAIL_4]);
		gtk_box_pack_start (GTK_BOX (hbox23), e_entry[EMAIL_4], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>WWW pages</b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		hbox24 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox24);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox24, TRUE, TRUE, 0);

		label = gtk_label_new (_("Website 1: "));
		gtk_widget_show (label);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox24), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[URL_1] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_1]), size_url-1);
		gtk_widget_show (e_entry[URL_1]);
		gtk_box_pack_start (GTK_BOX (hbox24), e_entry[URL_1], TRUE, TRUE, 2);

		hbox25 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox25);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox25, TRUE, TRUE, 0);

		label = gtk_label_new (_("Website 2: "));
		gtk_widget_show (label);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox25), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[URL_2] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_2]), size_url-1);
		gtk_widget_show (e_entry[URL_2]);
		gtk_box_pack_start (GTK_BOX (hbox25), e_entry[URL_2], TRUE, TRUE, 2);

		hbox26 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox26);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox26, TRUE, TRUE, 0);

		label = gtk_label_new (_("Website 3: "));
		gtk_widget_show (label);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox26), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[URL_3] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_3]), size_url-1);
		gtk_widget_show (e_entry[URL_3]);
		gtk_box_pack_start (GTK_BOX (hbox26), e_entry[URL_3], TRUE, TRUE, 2);

		hbox27 = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox27);
		gtk_box_pack_start (GTK_BOX (vbox3), hbox27, TRUE, TRUE, 0);

		label = gtk_label_new (_("Website 4: "));
		gtk_widget_show (label);
                gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox27), label, FALSE, FALSE, 4);
		gtk_size_group_add_widget(sg_label, label);

		e_entry[URL_4] = gtk_entry_new();
		gtk_entry_set_max_length(GTK_ENTRY(e_entry[URL_4]), size_url-1);
		gtk_widget_show (e_entry[URL_4]);
		gtk_box_pack_start (GTK_BOX (hbox27), e_entry[URL_4], TRUE, TRUE, 2);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox4), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

		label = gtk_label_new (NULL);
		gtk_widget_show (label);
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		sprintf (tmpbuf, _("<i><b>Additional info </b></i>"));
		gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

		vbox3 = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox3);
		gtk_container_add (GTK_CONTAINER (frame), vbox3);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

		frame = gtk_frame_new (NULL);
		gtk_widget_show (frame);
		gtk_box_pack_start (GTK_BOX (vbox3), frame, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
		gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_IN);

		scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_show (scrolledwindow2);
		gtk_container_add (GTK_CONTAINER (frame), scrolledwindow2);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

		notes_textinfo = gtk_text_buffer_new (NULL);
		gtk_text_buffer_get_iter_at_offset (notes_textinfo, &p_iter, 0);
		e_entry[NOTES] =  gtk_text_view_new_with_buffer (notes_textinfo);
		gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (e_entry[NOTES]), GTK_WRAP_WORD);
		gtk_widget_set_size_request (e_entry[NOTES], -1, 200);
		gtk_widget_show (e_entry[NOTES]);
		gtk_container_add (GTK_CONTAINER (scrolledwindow2),e_entry[NOTES]);
		gtk_container_set_border_width (GTK_CONTAINER (e_entry[NOTES]), 2);

		hseparator = gtk_hseparator_new();
		gtk_widget_show (hseparator);
		gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

		hbuttonbox = gtk_hbutton_box_new ();
		gtk_widget_show (hbuttonbox);
		gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox, FALSE, TRUE, 0);
		gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox), GTK_BUTTONBOX_END);
		gtk_box_set_spacing (GTK_BOX (hbuttonbox), 16);

		cancel_button = gtk_button_new_from_stock ("gtk-cancel");
		gtk_widget_show (cancel_button);
		g_signal_connect (G_OBJECT (cancel_button), "clicked",
							G_CALLBACK (gui_close_cancel_edit_window), NULL);
		gtk_container_add (GTK_CONTAINER (hbuttonbox), cancel_button);
		GTK_WIDGET_SET_FLAGS (cancel_button, GTK_CAN_DEFAULT);

		close_button = gtk_button_new_from_stock ("gtk-ok");
		gtk_widget_show (close_button);
		g_signal_connect (G_OBJECT (close_button), "clicked",
							G_CALLBACK (gui_close_edit_window), NULL);
		gtk_container_add (GTK_CONTAINER (hbuttonbox), close_button);
		GTK_WIDGET_SET_FLAGS (close_button, GTK_CAN_DEFAULT);

		gtk_widget_grab_focus (cancel_button);


		/* fill all fields */

		rdata = g_slist_nth_data (list, k);
		memcpy(&rec_entry, rdata, sizeof(recordstr));

		for(i=0; i < nfields-1; i++)
			gtk_entry_set_text(GTK_ENTRY(e_entry[i]), fields_ptr[i]);

		gtk_text_buffer_insert(notes_textinfo, &p_iter, fields_ptr[NOTES], -1);


		gtk_widget_show (edit_window);

		style = gtk_widget_get_style(GTK_WIDGET(edit_window));
		icon = gdk_pixmap_create_from_xpm_d (edit_window->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
		gdk_window_set_icon (edit_window->window, NULL, icon, icon_mask);

	} else {

		if(data_get_records())
			gui_show_status(_("Please select contact to edit first."));
		else
			gui_show_status(_("No contacts to edit."));

	}

}

/*------------------------------------------------------------------------------*/

