
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gui.h"
#include "prefs.h"
#include "icons.h"
#include "data.h"
#include "i18n.h"
#include "actions.h"
#include "add_edit.h"
#include "delete.h"
#include "about.h"
#include "export.h"

#include "icon.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>

static recordstr rec_entry;
PangoFontDescription *pfd_header;

/* main window */
GtkWidget				*toolbar;
GtkWidget				*main_window;
static GtkWidget		*vpaned;
static GtkWidget		*statusbar;
static GtkTextIter		iter;
static gint				context_id;
static GdkColor 		color;
GtkTextBuffer			*entry_textinfo = NULL;
GtkWidget				*text_sheet = NULL;
GtkWidget				*find_entry;
GtkWidget				*find_selector;

/* conatact list */
static GtkListStore		*store;
static GtkTreeIter 		titer;
GtkTreeModel 			*model;
static GtkWidget 		*treeview;
static GtkCellRenderer	*renderer;
GtkTreeViewColumn		*column_fn, *column_ln;

gchar current_fn[size_name], current_ln[size_name];
gchar tmpbuf[TMPBUF_SIZE];
gint current_records;

/*------------------------------------------------------------------------------*/

gchar* get_url_name(gint no)
{
gint k;
recordstr *rdata;

	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		rdata = g_slist_nth_data (list, k);

		if(no == 0)
			return rdata->url_1;
		else if(no == 1)
			return rdata->url_2;
		else if(no == 2)
			return rdata->url_3;
		else
			return rdata->url_4;

	} else

		return NULL;
}

/*------------------------------------------------------------------------------*/

gchar* get_email_name(gint no)
{
gint k;
recordstr *rdata;

	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		rdata = g_slist_nth_data (list, k);

		if(no == 0)
			return rdata->email_1;
		else if(no == 1)
			return rdata->email_2;
		else if(no == 2)
			return rdata->email_3;
		else
			return rdata->email_4;

	} else

		return NULL;
}

/*------------------------------------------------------------------------------*/

gint get_n_emails(void)
{
gint k, i;
recordstr *rdata;

	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		i = 0;
		rdata = g_slist_nth_data (list, k);

		if(strlen(rdata->email_1)) i++;
		if(strlen(rdata->email_2)) i++;
		if(strlen(rdata->email_3)) i++;
		if(strlen(rdata->email_4)) i++;

		return i;

	} else

		return 0;
}

/*------------------------------------------------------------------------------*/

gchar* gui_get_column_content (gint row, gint column)
{
GtkTreePath *path;
GtkTreeIter d_iter;
gpointer *otext = { (gpointer*)tmpbuf };

	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, row);
	gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &d_iter, path);
	gtk_tree_model_get(GTK_TREE_MODEL(model), &d_iter, column, otext, -1);
	gtk_tree_path_free (path);

	return otext[0];
}

/*------------------------------------------------------------------------------*/

void gui_list_set_row (gint row)
{
GtkTreePath *path;

	path = gtk_tree_path_new ();
	gtk_tree_path_append_index (path, row);
	gtk_tree_view_set_cursor (GTK_TREE_VIEW (treeview), path, NULL, TRUE);
	gtk_tree_path_free (path);

}


gint gui_list_get_row (void)
{
GtkTreePath *path;
gint *idx, row;

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (treeview), &path, NULL);

	idx = gtk_tree_path_get_indices(path);
	row = idx[0];

	return row;
}

/*------------------------------------------------------------------------------*/

void selection_made (GtkTreeSelection *selection, gpointer data)
{
GtkTreeIter s_iter;
GtkTreeModel *s_model;
gchar *fn_text, *ln_text;

	if (gtk_tree_selection_get_selected (selection, &s_model, &s_iter)) {

		gtk_tree_model_get (s_model, &s_iter, 0, &fn_text, -1);
		if(fn_text != NULL) {
			strcpy(current_fn, fn_text);
			g_free (fn_text);
		} else
			memset(current_fn, 0, size_name);


		gtk_tree_model_get (s_model, &s_iter, 1, &ln_text, -1);
		if(ln_text != NULL) {
			strcpy(current_ln, ln_text);
			g_free (ln_text);
		} else
			memset(current_ln, 0, size_name);

		gui_display_entry(current_fn, current_ln);

		gui_show_status(_("Idle"));

	}

}

/*------------------------------------------------------------------------------*/

void gui_jump_to_homepage(void)
{
	action_launch_website(DLUME_HOMEPAGE);
}

/*------------------------------------------------------------------------------*/

void gui_quick_search_activate(void)
{
	gtk_widget_grab_focus (find_entry);
	gui_show_status(_("Quick-search activated..."));
}

/*------------------------------------------------------------------------------*/

void gui_sswitch_callback(gpointer data, guint callback_action, GtkWidget *widget)
{

	if(config.find_type == 1)
		config.find_type = 0;
	else
		config.find_type = 1;

	gtk_option_menu_set_history (GTK_OPTION_MENU (find_selector), config.find_type);

}

/*------------------------------------------------------------------------------*/

void gui_navigate_callback(gpointer data, guint callback_action, GtkWidget *widget)
{
gint cr, rc;

	rc = current_records - 1;

	if (rc > 0) {

		if(gtk_widget_is_focus(treeview)==FALSE)
			gtk_widget_grab_focus(treeview);

		cr = gui_list_get_row ();

		switch(callback_action) {

			case 1:	/* next */
				if(cr < rc) cr++;
				break;

			case 2:	/* previous */
				if(cr > 0) cr--;
				break;

			case 3:	/* first */
				cr = 0;
				break;

			case 4:	/* last */
				cr = rc;
				break;

			default:
				break;
		}

		gui_list_set_row(cr);

	}
}

/*------------------------------------------------------------------------------*/

void gui_display_entry (gchar *first_name, gchar *last_name)
{
gint i, j, k, l, n;
recordstr *rdata;
GSList *node;
GtkTextIter a_iter, b_iter;
GtkTextChildAnchor *anchor;

gpointer fields_ptr_1[]= {
	rec_entry.first_name, rec_entry.last_name, rec_entry.nickname, rec_entry.birthday,
	rec_entry.address, rec_entry.post_code,
	rec_entry.city, rec_entry.state, rec_entry.country
};

gpointer fields_ptr_2[]= {
	rec_entry.home_phone_1, rec_entry.home_phone_2,
	rec_entry.home_phone_3, rec_entry.home_phone_4,
	rec_entry.work_phone_1, rec_entry.work_phone_2, rec_entry.work_phone_3,
	rec_entry.work_phone_4,  rec_entry.cell_phone_1,
	rec_entry.cell_phone_2, rec_entry.cell_phone_3, rec_entry.cell_phone_4,
	rec_entry.email_1, rec_entry.email_2, rec_entry.email_3,
	rec_entry.email_4, rec_entry.url_1, rec_entry.url_2,
	rec_entry.url_3, rec_entry.url_4
};

gchar *vt_names_1[] = {
	_("FIRST NAME: "), _("LAST NAME: "), _("NICKNAME: "), _("BIRTHDAY DATE: "),
	_("ADDRESS: "), _("POST CODE: "), _("CITY: "), _("STATE: "), _("COUNTRY: ")
};

gchar *vt_names_2[] = {
	_("HOME PHONES:\n"), _("WORK PHONES:\n"), _("CELL PHONES:\n"),
	_("E-MAILS:\n"), _("WEBSITES:\n")
};

gchar *vt_names_3[] = {
	_("HOME PHONE: "), _("WORK PHONE: "), _("CELL PHONE: "),
	_("E-MAIL:\n"), _("WEBSITE:\n")
};


	i = find_record(first_name, last_name);

	if (i != -1) {

		node = g_slist_nth(list, i);
		rdata = node->data;
		memcpy(&rec_entry, rdata, sizeof(recordstr));

		if (rdata) {

			gtk_text_buffer_get_bounds (entry_textinfo, &a_iter, &b_iter);
			gtk_text_buffer_delete (entry_textinfo, &a_iter, &b_iter);

			gtk_text_buffer_get_iter_at_offset (entry_textinfo, &iter, 0);

			for(i = 0; i < HOME_PHONE_1; i++) {

				if(strlen(fields_ptr_1[i])) {

					gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									(gchar *) vt_names_1[i], -1, "value_title", NULL);

					snprintf(tmpbuf, TMPBUF_SIZE, "%s\n", (gchar *) fields_ptr_1[i]);
					gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
										tmpbuf, -1, "value", NULL);

					if ((i==LAST_NAME && strlen(fields_ptr_1[LAST_NAME])) ||
						(i==BIRTHDAY && strlen(fields_ptr_1[BIRTHDAY])) ||
						(i==NICKNAME && strlen(fields_ptr_1[NICKNAME])))
						gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
										"\n", -1, "value", NULL);
				}
			}

			for(k = 0, j = ADDRESS; j < HOME_PHONE_1; j++)
				k += strlen(fields_ptr_1[j]);

			if(k)
				gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									"\n", -1, "value", NULL);

			for(i=0; i < 5; i++) {   /* HP+WP+CP+EM+URL  = 5 */

				for(k = j = n = 0; j < 4; j++) {
					l = strlen(fields_ptr_2[i*4+j]);
					if(l) n++;
					k += l;
				}

				if(n > 1) {
					gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									(gchar *) vt_names_2[i], -1, "value_title", NULL);
				} else {
					if(n) gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									(gchar *) vt_names_3[i], -1, "value_title", NULL);
				}

				if(i < 3) {

					/* phones */

					for(j = 0; j < 4; j++) {
						if(strlen(fields_ptr_2[i*4+j])) {
							snprintf(tmpbuf, TMPBUF_SIZE, "%s\n", (gchar *) fields_ptr_2[i*4+j]);
							gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
												tmpbuf, -1, "value", NULL);
						}
					}

				} else {

					/* e-mails, urls */

					for(j = 0; j < 4; j++) {
						if(strlen(fields_ptr_2[i*4+j])) {
							anchor = gtk_text_buffer_create_child_anchor (entry_textinfo, &iter);
							gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
													"\n", -1, "value", NULL);
						}
					}
				}


				if(k) gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
										"\n", -1, "value", NULL);

			}

			/* additional info */

			if(strlen(rec_entry.notes)) {
				gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									_("ADDITIONAL INFO:\n"), -1, "value_title", NULL);
				snprintf(tmpbuf, TMPBUF_SIZE, "%s\n", rec_entry.notes);
				gtk_text_buffer_insert_with_tags_by_name (entry_textinfo, &iter,
									tmpbuf, -1, "value", NULL);
			}

		}
	}

	gui_attach_widgets (GTK_TEXT_VIEW(text_sheet));

}

/*------------------------------------------------------------------------------*/

void gui_show_status (gchar *message)
{
	gtk_statusbar_pop (GTK_STATUSBAR(statusbar), context_id);
	gtk_statusbar_push (GTK_STATUSBAR(statusbar), context_id, message);
}

/*------------------------------------------------------------------------------*/
/* find selector callback */

void find_type_selected(GtkWidget *item, GtkUpdateType type)
{
	config.find_type = type;
}

/*------------------------------------------------------------------------------*/
/* close window callback */

void gui_close_window( GtkWidget *widget, gpointer data )
{
	gtk_window_get_size (GTK_WINDOW(main_window),
						&config.window_size_x, &config.window_size_y);

	if(config.restore_windows_pos)
		gdk_window_get_root_origin (main_window->window,
							&config.window_x, &config.window_y);

	config.pane_pos = gtk_paned_get_position(GTK_PANED (vpaned));

	gtk_accel_map_save(s_getfilename_config_a ());

	gtk_main_quit ();
}

/*------------------------------------------------------------------------------*/
/* key press callback */

int gui_fe_rkey_press (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	current_records = gui_update_records_list((gchar *)gtk_entry_get_text(GTK_ENTRY(widget)));
	gui_list_set_row (0);
	return FALSE;
}

int gui_fe_key_press (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	if (event->keyval == GDK_Escape)
		gtk_entry_set_text(GTK_ENTRY(widget), "");

	return FALSE;
}
/*------------------------------------------------------------------------------*/
/* double-click callback */

void row_activated_cb (GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column)
{
	gui_edit_contact_window();
}

/*------------------------------------------------------------------------------*/
/* e-mail, url links callback */

void link_callback (GtkWidget *button, gpointer data)
{
gint i, j;

	i = (gint) data;
	j = get_n_emails();

	if(i < j)
		action_send_email(get_email_name(i));
	else
		action_launch_website(get_url_name(i-j));

}

/*------------------------------------------------------------------------------*/

gboolean gui_find_anchor (GtkTextIter *f_iter)
{
	while (gtk_text_iter_forward_char (f_iter)) {
		if (gtk_text_iter_get_child_anchor (f_iter))
			return TRUE;
	}

	return FALSE;
}

/*------------------------------------------------------------------------------*/

void gui_attach_widgets (GtkTextView *text_view)
{
GtkTextBuffer *buffer;
gint i, j;
GtkTextChildAnchor *anchor;
GtkWidget *widget, *label;
gchar tmpbuffer[size_email];
PangoFontDescription *pfda_header;


	buffer = gtk_text_view_get_buffer (text_view);
	gtk_text_buffer_get_start_iter (buffer, &iter);

	i = 0;
	j = get_n_emails();

	while (gui_find_anchor (&iter)) {

		anchor = gtk_text_iter_get_child_anchor (&iter);

		widget = gtk_button_new();
		label = gtk_label_new ("");
		gtk_widget_show (label);
		gtk_label_set_use_markup (GTK_LABEL(label), TRUE);
		gtk_container_add (GTK_CONTAINER (widget), label);

		/* change font */
		pfda_header = pango_font_description_from_string(config.font_header);
		gtk_widget_modify_font (GTK_WIDGET(label), pfda_header);
		pango_font_description_free (pfda_header);

		if(i < j) {
			sprintf(tmpbuffer, "%s", get_email_name(i));
			gdk_color_parse (config.color3, &color);
		} else {
			sprintf(tmpbuffer, "%s", get_url_name(i-j));
			gdk_color_parse (config.color4, &color);
		}

		/* change color */
		gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, &color);
		gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_ACTIVE, &color);
		gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_PRELIGHT, &color);

		gtk_label_set_markup (GTK_LABEL (label), tmpbuffer);
		gtk_button_set_relief (GTK_BUTTON (widget), GTK_RELIEF_NONE);

		g_signal_connect (widget, "clicked", G_CALLBACK (link_callback), (gpointer)i);

		gtk_text_view_add_child_at_anchor (text_view, widget, anchor);
		gtk_widget_show_all (widget);

		i++;
	}
}

/*------------------------------------------------------------------------------*/

void gui_create_window(void)
{
GtkWidget			*vbox1;
GtkWidget			*menubar;
GtkWidget			*add_button;
GtkWidget			*edit_button;
GtkWidget			*delete_button;
GtkWidget			*quit_button;
GtkWidget			*vbox2;
GtkWidget			*hbox1;
GtkWidget			*vbox3;
GtkWidget			*scrolledwindow1;
GtkWidget			*scrolledwindow2;
GtkItemFactory		*item_factory;
GtkAccelGroup		*accel_group;
GtkWidget			*option_menu;
GtkWidget			*menuitem;
GtkWidget			*frame;
GtkWidget			*picon;
GdkPixmap			*pixmap, *icon;
GdkBitmap			*mask, *icon_mask;
GtkStyle			*style;
GtkTreeSelection	*select;
GtkWidget			*handlebox;
gchar				tmpbuf[256];

GtkItemFactoryEntry menu_items[] = {
  { _("/_File"),						NULL,			NULL,							0, "<Branch>" },
  { _("/File/tearoff"),					NULL,			NULL,							0, "<Tearoff>" },
  { _("/File/Select fields to export"), NULL,			export_select_fields,			0, "<StockItem>", GTK_STOCK_INDEX },
  { _("/File/Export as"),				NULL,			NULL,							0, "<Branch>" },
  { _("/File/Export as/CSV file..."), 	NULL,			export_csv_select_file,			0, "<StockItem>", GTK_STOCK_SAVE_AS },
  { _("/File/Export as/HTML file..."),	NULL,			export_html_select_file,		0, "<StockItem>", GTK_STOCK_SAVE_AS },
  { _("/File/sep1"),					NULL,			NULL,							0, "<Separator>" },
  { _("/File/Preferences"),				"<ctrl>p",		gui_create_prefs_window,		0, "<StockItem>", GTK_STOCK_PREFERENCES },
  { _("/File/sep1"),					NULL,			NULL,							0, "<Separator>" },
  { _("/File/Quit"),					"<ctrl>q",		gui_close_window,				0, "<StockItem>", GTK_STOCK_QUIT },
  { _("/_Contact"),						NULL,			NULL,							0, "<Branch>" },
  { _("/Contact/tearoff"),				NULL,			NULL,							0, "<Tearoff>" },
  { _("/Contact/Add"),					"<ctrl>a",		gui_create_new_contact_window,	0, "<StockItem>", GTK_STOCK_ADD },
  { _("/Contact/Edit"),					"<ctrl>e",		gui_edit_contact_window,		0, "<StockItem>", GTK_STOCK_CONVERT },
  { _("/Contact/Delete"),				"<ctrl>d",		gui_create_delete_window,		0, "<StockItem>", GTK_STOCK_DELETE },
  { _("/Contact/sep1"),					NULL,			NULL,							0, "<Separator>" },
  { _("/Contact/Quick search"),			"<ctrl>l",		gui_quick_search_activate,		0, "<StockItem>", GTK_STOCK_FIND },
  { _("/Contact/Switch search mode"),	"F1",			gui_sswitch_callback,			0, "<StockItem>", GTK_STOCK_REFRESH },
  { _("/_Go"),							NULL,			NULL,							0, "<Branch>" },
  { _("/Go/tearoff"),					NULL,			NULL,							0, "<Tearoff>" },
  { _("/Go/Next"),						"<ctrl>Right",	gui_navigate_callback,			1, "<StockItem>", GTK_STOCK_GO_FORWARD },
  { _("/Go/Previous"),					"<ctrl>Left",	gui_navigate_callback,			2, "<StockItem>", GTK_STOCK_GO_BACK },
  { _("/Go/sep1"),						NULL,         	NULL,							0, "<Separator>" },
  { _("/Go/First"),						"<ctrl>Home",	gui_navigate_callback,			3, "<StockItem>", GTK_STOCK_GOTO_FIRST },
  { _("/Go/Last"),						"<ctrl>End",	gui_navigate_callback,			4, "<StockItem>", GTK_STOCK_GOTO_LAST },
  { _("/_Help"),						NULL,			NULL,							0, "<LastBranch>" },
  { _("/Help/tearoff"),					NULL,			NULL,							0, "<Tearoff>" },
  { _("/Help/About"),					NULL,			gui_create_about_window,		0, "<StockItem>", GTK_STOCK_HOME },
  { _("/Help/Show license"),			NULL,			gui_create_license_window,		0, "<StockItem>", GTK_STOCK_DIALOG_INFO },
  { _("/Help/sep1"),					NULL,			NULL,							0, "<Separator>" },
  { _("/Help/Dlume homepage"),			NULL,			gui_jump_to_homepage,			0, "<StockItem>", GTK_STOCK_JUMP_TO },
};

gint nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);

	pfd_header = pango_font_description_from_string(config.font_header);

	main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	sprintf(tmpbuf, _("Dlume %s"), VERSION);
	gtk_window_set_title (GTK_WINDOW (main_window), tmpbuf);

	gtk_window_set_default_size (GTK_WINDOW(main_window),
							config.window_size_x, config.window_size_y);

	gtk_window_set_resizable (GTK_WINDOW (main_window), TRUE);

	gtk_container_set_border_width(GTK_CONTAINER (main_window), 1);

	if(config.restore_windows_pos)
		gtk_window_move (GTK_WINDOW (main_window),
							config.window_x, config.window_y);

	g_signal_connect (G_OBJECT (main_window), "delete_event",
						G_CALLBACK(gui_close_window), NULL);

	gtk_widget_show (main_window);

	/***[ MENU ]*************************************************************/

	accel_group = gtk_accel_group_new ();

	item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", accel_group);

	gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL);
	gtk_window_add_accel_group (GTK_WINDOW (main_window), accel_group);

	gtk_accel_map_load(s_getfilename_config_a ());

	menubar = gtk_item_factory_get_widget (item_factory, "<main>");

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (main_window), vbox1);

	handlebox = gtk_handle_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox1), handlebox, FALSE, TRUE, 0);
	gtk_widget_show (handlebox);

	gtk_container_add (GTK_CONTAINER (handlebox), menubar);
	gtk_widget_show (menubar);

	/***[ TOOLBAR ]**********************************************************/

	handlebox = gtk_handle_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox1), handlebox, FALSE, TRUE, 1);
	gtk_widget_show (handlebox);

	toolbar = gtk_toolbar_new ();
	gtk_container_add (GTK_CONTAINER (handlebox), toolbar);

	gtk_widget_show (toolbar);
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), config.toolbar_style);

	pixmap = gdk_pixmap_create_from_xpm_d(main_window->window, &mask, NULL, add_xpm);
	picon = gtk_image_new_from_pixmap (pixmap, mask);

	add_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar),
								GTK_TOOLBAR_CHILD_BUTTON,
								NULL,
								_("Add"),
								NULL, NULL,
								picon, G_CALLBACK(gui_create_new_contact_window), NULL);
	gtk_label_set_use_underline (GTK_LABEL (((GtkToolbarChild*) (g_list_last (GTK_TOOLBAR (toolbar)->children)->data))->label), TRUE);
	gtk_widget_show (add_button);
	gtk_container_set_border_width (GTK_CONTAINER (add_button), 1);

	pixmap = gdk_pixmap_create_from_xpm_d(main_window->window, &mask, NULL, edit_xpm);
	picon = gtk_image_new_from_pixmap (pixmap, mask);

	edit_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar),
								GTK_TOOLBAR_CHILD_BUTTON,
								NULL,
								_("Edit"),
								NULL, NULL,
								picon, G_CALLBACK(gui_edit_contact_window), NULL);
	gtk_label_set_use_underline (GTK_LABEL (((GtkToolbarChild*) (g_list_last (GTK_TOOLBAR (toolbar)->children)->data))->label), TRUE);
	gtk_widget_show (edit_button);
	gtk_container_set_border_width (GTK_CONTAINER (edit_button), 1);

	pixmap = gdk_pixmap_create_from_xpm_d(main_window->window, &mask, NULL, delete_xpm);
  	picon = gtk_image_new_from_pixmap (pixmap, mask);

	delete_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar),
								GTK_TOOLBAR_CHILD_BUTTON,
								NULL,
								_("Delete"),
								NULL, NULL,
								picon, G_CALLBACK(gui_create_delete_window), NULL);
	gtk_label_set_use_underline (GTK_LABEL (((GtkToolbarChild*) (g_list_last (GTK_TOOLBAR (toolbar)->children)->data))->label), TRUE);
	gtk_widget_show (delete_button);
	gtk_container_set_border_width (GTK_CONTAINER (delete_button), 1);

	pixmap = gdk_pixmap_create_from_xpm_d(main_window->window, &mask, NULL, quit_xpm);
	picon = gtk_image_new_from_pixmap (pixmap, mask);

	gtk_toolbar_append_space(GTK_TOOLBAR (toolbar));

	quit_button = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar),
								GTK_TOOLBAR_CHILD_BUTTON,
								NULL,
								_("Quit!"),
								NULL, NULL,
								picon, G_CALLBACK(gui_close_window), NULL);
	gtk_label_set_use_underline (GTK_LABEL (((GtkToolbarChild*) (g_list_last (GTK_TOOLBAR (toolbar)->children)->data))->label), TRUE);
	gtk_widget_show (quit_button);
	gtk_container_set_border_width (GTK_CONTAINER (quit_button), 1);

	/***[ FIND SELECTOR ]****************************************************/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox1, FALSE, TRUE, 0);

	find_selector = gtk_option_menu_new ();
	gtk_widget_show (find_selector);
	gtk_box_pack_start (GTK_BOX (hbox1), find_selector, FALSE, FALSE, 0);

	option_menu = gtk_menu_new ();
	menuitem = gtk_menu_item_new_with_label (_("First name"));
	gtk_widget_show (menuitem);
        g_signal_connect (G_OBJECT (menuitem), "activate",
						G_CALLBACK (find_type_selected), (gpointer*)0);

	gtk_menu_shell_append (GTK_MENU_SHELL (option_menu), menuitem);

	menuitem = gtk_menu_item_new_with_label (_("Last name"));
	gtk_widget_show (menuitem);
        g_signal_connect (G_OBJECT (menuitem), "activate",
						G_CALLBACK (find_type_selected), (gpointer*)1);

	gtk_menu_shell_append (GTK_MENU_SHELL (option_menu), menuitem);

	gtk_option_menu_set_menu (GTK_OPTION_MENU (find_selector), option_menu);

	find_entry = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(find_entry), size_name);
	gtk_widget_show (find_entry);
	g_signal_connect (G_OBJECT(find_entry), "key_release_event",
						G_CALLBACK(gui_fe_rkey_press), NULL);
	g_signal_connect (G_OBJECT(find_entry), "key_press_event",
						G_CALLBACK(gui_fe_key_press), NULL);
	gtk_box_pack_start (GTK_BOX (hbox1), find_entry, TRUE, TRUE, 0);

	/***[ CONTACT LIST ]*****************************************************/

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_box_pack_start (GTK_BOX (vbox2), vbox3, TRUE, TRUE, 0);

	vpaned = gtk_vpaned_new ();
	gtk_widget_show (vpaned);
	gtk_box_pack_start (GTK_BOX (vbox3), vpaned, TRUE, TRUE, 0);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_paned_add1 (GTK_PANED (vpaned), frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 3);

	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (frame), scrolledwindow1);
	gtk_widget_show (scrolledwindow1);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	model = GTK_TREE_MODEL(store);

	treeview = gtk_tree_view_new_with_model (model);
	g_signal_connect (treeview, "row_activated", G_CALLBACK (row_activated_cb), model);
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), 2);
	gtk_tree_view_set_search_column (GTK_TREE_VIEW (treeview), 2);
	gtk_container_add(GTK_CONTAINER (scrolledwindow1), treeview);
	gtk_widget_show (treeview);
	
	renderer = gtk_cell_renderer_text_new ();

	column_fn = gtk_tree_view_column_new_with_attributes (_("First name"),
							renderer, "text", 0, NULL);
	gtk_tree_view_column_set_sort_column_id (column_fn, 0);
	gtk_tree_view_column_set_min_width(GTK_TREE_VIEW_COLUMN (column_fn), 160);
	gtk_tree_view_column_set_resizable(GTK_TREE_VIEW_COLUMN (column_fn), TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), column_fn);

	column_ln = gtk_tree_view_column_new_with_attributes (_("Last name"),
							renderer, "text", 1, NULL);
	gtk_tree_view_column_set_sort_column_id (column_ln, 1);
	gtk_tree_view_column_set_resizable(GTK_TREE_VIEW_COLUMN (column_ln), TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), column_ln);

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	g_signal_connect(G_OBJECT(select), "changed",
						G_CALLBACK(selection_made), NULL);

	/************************************************************************/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_paned_add2 (GTK_PANED (vpaned), frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 3);

	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow2);
	gtk_container_add (GTK_CONTAINER (frame), scrolledwindow2);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	entry_textinfo = gtk_text_buffer_new (NULL);
	gtk_text_buffer_get_iter_at_offset (entry_textinfo, &iter, 0);

	text_sheet = gtk_text_view_new_with_buffer (entry_textinfo);
	gtk_container_set_border_width (GTK_CONTAINER (text_sheet), 2);
	gtk_text_view_set_editable(GTK_TEXT_VIEW (text_sheet), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW (text_sheet), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_sheet), GTK_WRAP_WORD);
	gtk_widget_show (text_sheet);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), text_sheet);

	/* set font */

	gtk_widget_modify_font (text_sheet, pfd_header);

	/* set background color */

	gdk_color_parse (config.color5, &color);
	gtk_widget_modify_base (text_sheet, GTK_STATE_NORMAL, &color);
	gtk_widget_modify_bg (text_sheet, GTK_STATE_NORMAL, &color);

	/* set some tags */

	gdk_color_parse (config.color1, &color);

	gtk_text_buffer_create_tag (entry_textinfo, "value_title",
					"foreground-gdk", &color,
					"weight", PANGO_WEIGHT_BOLD, NULL);

	gdk_color_parse (config.color2, &color);

	gtk_text_buffer_create_tag (entry_textinfo, "value",
					"foreground-gdk", &color, NULL);


	/***[ STATUSBAR ]********************************************************/

	statusbar = gtk_statusbar_new ();
	gtk_widget_show (statusbar);
	gtk_box_pack_start (GTK_BOX (vbox2), statusbar, FALSE, FALSE, 0);

	context_id = gtk_statusbar_get_context_id( GTK_STATUSBAR(statusbar), "DLUME");
	sprintf(tmpbuf, "%s (%d contacts found)", WELCOME_TEXT, records);

	if(records)
		gtk_statusbar_push (GTK_STATUSBAR(statusbar), context_id, tmpbuf);
	else
		gtk_statusbar_push (GTK_STATUSBAR(statusbar), context_id, WELCOME_TEXT);

	/***[ RESTORE VALUES ]***************************************************/

	gtk_paned_set_position (GTK_PANED (vpaned), config.pane_pos);
	gtk_option_menu_set_history (GTK_OPTION_MENU (find_selector), config.find_type);

	if(config.sorting_mode)
		g_signal_emit_by_name (column_ln, "clicked");
	else
		g_signal_emit_by_name (column_fn, "clicked");

	style = gtk_widget_get_style(GTK_WIDGET(main_window));
	icon = gdk_pixmap_create_from_xpm_d (main_window->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
	gdk_window_set_icon (main_window->window, NULL, icon, icon_mask);

	current_records = gui_display_records_list();

	if(config.find_activate)
		gui_quick_search_activate();

		if(data_get_records() > 0)
			gui_list_set_row(0);

	pango_font_description_free (pfd_header);

}

/*------------------------------------------------------------------------------*/

gint gui_display_records_list(void)
{
GSList *node;
recordstr *rdata;
gint recs;

	/* clear list */

	gtk_list_store_clear (store);
        recs = 0;

	/* add to list */

	for (node = list; node; node = node->next) {

		rdata = node->data;

		if(rdata) {

			if(strlen(rdata->first_name) && strlen(rdata->last_name)) {
				recs++;
				gtk_list_store_append (store, &titer);
				gtk_list_store_set (store, &titer, 0, rdata->first_name, 1, rdata->last_name, -1);
			}

			else if(strlen(rdata->first_name)) {
				recs++;
				gtk_list_store_append (store, &titer);
				gtk_list_store_set (store, &titer, 0, rdata->first_name, -1);
			}

			else if(strlen(rdata->last_name)) {
				recs++;
				gtk_list_store_append (store, &titer);
				gtk_list_store_set (store, &titer, 1, rdata->last_name, -1);
			}

		}
	}

	return recs;
}

/*------------------------------------------------------------------------------*/

gint gui_update_records_list(gchar *value)
{
GSList *node;
recordstr *rdata;
gint len, recs;
gchar tmpb[size_name];

	len = strlen(value);

	/* clear list */

	gtk_list_store_clear (store);
	recs = 0;

	/* add to list */

	for (node = list; node; node = node->next) {

		rdata = node->data;

		if(rdata) {

			if(config.find_type)
				strcpy(tmpb, rdata->last_name);
			else
				strcpy(tmpb, rdata->first_name);


			if(!strncmp(g_utf8_casefold(value, len), g_utf8_casefold(tmpb, strlen(tmpb)), len)) {

				if(strlen(rdata->first_name) && strlen(rdata->last_name)){
					recs++;
					gtk_list_store_append (store, &titer);
					gtk_list_store_set (store, &titer, 0, rdata->first_name, 1, rdata->last_name, -1);
				}

				else if(strlen(rdata->first_name)){
					recs++;
					gtk_list_store_append (store, &titer);
					gtk_list_store_set (store, &titer, 0, rdata->first_name, -1);
				}

				else if(strlen(rdata->last_name)){
					recs++;
					gtk_list_store_append (store, &titer);
					gtk_list_store_set (store, &titer, 1, rdata->last_name, -1);
				}

			}
		}
	}

	return recs;
}

/*------------------------------------------------------------------------------*/

GtkWidget* gui_stock_label_button(gchar *blabel, const gchar *bstock)
{
GtkWidget	*button;
GtkWidget	*alignment;
GtkWidget	*hbox;
GtkWidget	*image;

	button = g_object_new (GTK_TYPE_BUTTON, "visible", TRUE, NULL);

	alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	hbox = gtk_hbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (alignment), hbox);

	image = gtk_image_new_from_stock (bstock, GTK_ICON_SIZE_BUTTON);

	if (image)
		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (hbox),
	g_object_new (GTK_TYPE_LABEL, "label", blabel, "use_underline", TRUE, NULL), FALSE, TRUE, 0);
	gtk_widget_show_all (alignment);
	gtk_container_add (GTK_CONTAINER (button), alignment);

	return button;
}

/*------------------------------------------------------------------------------*/

GtkWidget* gui_stock_button(const gchar *bstock)
{
GtkWidget	*button;
GtkWidget	*alignment;
GtkWidget	*hbox;
GtkWidget	*image;

	button = g_object_new (GTK_TYPE_BUTTON, "visible", TRUE, NULL);

	alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	hbox = gtk_hbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (alignment), hbox);

	image = gtk_image_new_from_stock (bstock, GTK_ICON_SIZE_BUTTON);

	if (image)
		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);

	gtk_widget_show_all (alignment);
	gtk_container_add (GTK_CONTAINER (button), alignment);

	return button;
}

/*------------------------------------------------------------------------------*/

