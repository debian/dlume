
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>

#define	WELCOME_TEXT	"Welcome to Dlume !"
#define	DLUME_HOMEPAGE	"http://clay.ll.pl/dlume.html"
#define	TMPBUF_SIZE		1024

typedef enum {
	EMAIL = 0,
	URL,
} link_type;

void		gui_create_window			(void);
void		gui_show_status				(gchar *message);
void		gui_create_prefs_window		(void);
void		gui_set_button_bg_color		(GtkWidget *widget, gchar *icolor);
void		gui_display_entry			(gchar *first_name, gchar *last_name);
gint		gui_display_records_list	(void);
gint		gui_update_records_list		(gchar *value);
gchar*		gui_get_column_content 		(gint row, gint column);
GtkWidget*	gui_stock_label_button		(gchar *blabel, const gchar *bstock);
GtkWidget*	gui_stock_button			(const gchar *bstock);
void		gui_list_set_row			(gint row);
gint		gui_list_get_row			(void);
void		gui_attach_widgets			(GtkTextView *text_view);

extern		GtkWidget			*main_window;
extern		GtkWidget			*toolbar;
extern		GtkTextBuffer		*entry_textinfo;
extern		GtkWidget			*text_sheet;
extern		GtkWidget			*find_entry;
extern		gchar				current_fn[], current_ln[];
extern		GtkTreeViewColumn	*column_fn, *column_ln;
extern		GtkTreeModel 		*model;
extern		gchar				*icon_xpm[];


