
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#define		DATAFILE		"dlume-data.xml"

#define		size_name			25
#define		size_address		50
#define		size_city			25
#define		size_country		25
#define		size_state			25
#define		size_post_code		15
#define		size_phone			45
#define		size_email			40
#define		size_url			50
#define		size_birthday		25
#define		size_notes			4096

#define		nfields				30		/* number of fields */

typedef struct
{
	gchar		first_name[size_name];
	gchar		last_name[size_name];
	gchar		nickname[size_name];
	gchar		birthday[size_birthday];
	gchar		address[size_address];
	gchar		post_code[size_post_code];
	gchar		city[size_city];
	gchar		state[size_state];
	gchar		country[size_country];

	gchar		home_phone_1[size_phone];
	gchar		home_phone_2[size_phone];
	gchar		home_phone_3[size_phone];
	gchar		home_phone_4[size_phone];

	gchar		work_phone_1[size_phone];
	gchar		work_phone_2[size_phone];
	gchar		work_phone_3[size_phone];
	gchar		work_phone_4[size_phone];

	gchar		cell_phone_1[size_phone];
	gchar		cell_phone_2[size_phone];
	gchar		cell_phone_3[size_phone];
	gchar		cell_phone_4[size_phone];

	gchar		email_1[size_email];
	gchar		email_2[size_email];
	gchar		email_3[size_email];
	gchar		email_4[size_email];

	gchar		url_1[size_url];
	gchar		url_2[size_url];
	gchar		url_3[size_url];
	gchar		url_4[size_url];

	gchar		notes[size_notes];

} recordstr;


gint data_add_record		(recordstr *entry);
gint data_show				(void);
gint data_init				(void);
gint data_close				(void);
gint data_write				(void);
gint data_read				(void);
gint fill_record			(recordstr *entry);
gint clear_record			(recordstr *entry);
gint find_record			(gchar *f, gchar *l);
gint data_remove_record		(gint k);
gint data_get_records		(void);
void data_print_csv_entry	(FILE *filehandle, recordstr *rdata);
void data_print_html_entry	(FILE *filehandle, recordstr *rdata);

extern gint records;
extern GSList *list;
extern gint fields_size[];

enum {
	FIRST_NAME = 0,
	LAST_NAME,
	NICKNAME,
	BIRTHDAY,
	ADDRESS,
	POST_CODE,
	CITY,
	STATE,
	COUNTRY,
	HOME_PHONE_1,
	HOME_PHONE_2,
	HOME_PHONE_3,
	HOME_PHONE_4,
	WORK_PHONE_1,
	WORK_PHONE_2,
	WORK_PHONE_3,
	WORK_PHONE_4,
	CELL_PHONE_1,
	CELL_PHONE_2,
	CELL_PHONE_3,
	CELL_PHONE_4,
	EMAIL_1,
	EMAIL_2,
	EMAIL_3,
	EMAIL_4,
	URL_1,
	URL_2,
	URL_3,
	URL_4,
	NOTES
};


