
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "prefs.h"

struct dlumeprefs config;

FILE *prefs_filehandle;

/*---------------------------------------------------------------------------*/

gchar* s_getdir_config (void)
{
static gchar cfgdir[MAX_PATH];
struct stat cfg;

	s_strcpy (cfgdir, getenv ("HOME"), MAX_PATH);
	s_strcat (cfgdir, "/.clay", MAX_PATH);

	if(stat (cfgdir, &cfg) < 0)
		mkdir (cfgdir, S_IRUSR | S_IWUSR | S_IXUSR);

	return cfgdir;
}

/*---------------------------------------------------------------------------*/

gchar* s_getfilename_config (void)
{
static gchar filename[MAX_PATH];

	s_strcpy (filename, s_getdir_config(), MAX_PATH);
	s_strcat (filename, slash, MAX_PATH);
	s_strcat (filename, config_filename, MAX_PATH);

	return filename;
}

/*---------------------------------------------------------------------------*/

gchar* s_getfilename_config_a (void)
{
static gchar filename[MAX_PATH];

	s_strcpy (filename, s_getdir_config(), MAX_PATH);
	s_strcat (filename, slash, MAX_PATH);
	s_strcat (filename, config_a_filename, MAX_PATH);

	return filename;
}

/*---------------------------------------------------------------------------*/

void* s_prefs_openfile (gchar *filename, gint openmode)
{
	prefs_filehandle = NULL;

	if (openmode == P_READ)
		prefs_filehandle = fopen (filename, "rb");
	else if (openmode == P_WRITE)
		prefs_filehandle = fopen (filename, "wb");

	return prefs_filehandle;
}


void s_prefs_closefile (void)
{
	fclose (prefs_filehandle);
}

/*---------------------------------------------------------------------------*/

void s_prefs_put_int (gchar *tagname, gint value)
{
	fprintf (prefs_filehandle, "%s = %d\n", tagname, value);
}


void s_prefs_put_float (gchar *tagname, gfloat value)
{
	fprintf (prefs_filehandle, "%s = %f\n", tagname, value);
}


void s_prefs_put_string (gchar *tagname, gchar *value)
{
	fprintf (prefs_filehandle, "%s = %s\n", tagname, value);
}


void s_prefs_put_lf (void)
{
	fprintf (prefs_filehandle, "\n");
}


void s_prefs_put_comment (gchar *comment)
{
gchar text[MAX_LINE_LEN];

	s_strcpy (text, "# ", MAX_LINE_LEN);
	s_strcat (text, comment, MAX_LINE_LEN);
	fprintf (prefs_filehandle, text);
}

/*---------------------------------------------------------------------------*/

gchar* s_prefs_get_line_with_tag (gchar *tagname)
{
static gchar prfline[MAX_LINE_LEN];
gint i;
gchar c;

	fseek (prefs_filehandle, 0, SEEK_SET);

	while (!feof (prefs_filehandle)) {
		i = 0;

		while (((c = fgetc (prefs_filehandle)) != crlf_char) && c!= EOF && i < MAX_LINE_LEN)
			prfline[i++] = c;

		prfline[i] = null_char;

		if (prfline[0] != '#')
			if (!strncmp (tagname, prfline, strlen (tagname))) break;
	}

	return prfline;
}


gchar* s_prefs_get_value_field (gchar *tagname)
{
static gchar valuestr[MAX_VALUE_LEN];
gchar *valpos, c;
gint i;

	i = 0;

	if ((valpos = strchr (s_prefs_get_line_with_tag (tagname), '='))) {
		while((c = valpos[i+2]) != null_char && i < MAX_VALUE_LEN) valuestr[i++] = c;
	}

	valuestr[i] = null_char;
	return valuestr;
}


gint s_prefs_get_int (gchar *tagname)
{
	return (atoi (s_prefs_get_value_field (tagname)));
}


gfloat s_prefs_get_float (gchar *tagname)
{
	return (atof (s_prefs_get_value_field (tagname)));
}


gchar* s_prefs_get_string (gchar *tagname)
{
	return (s_prefs_get_value_field (tagname));
}

/*---------------------------------------------------------------------------*/
/* following functions based on samba sources.                               */
/* safe_strcpy and safe_strcat routines written by Andrew Tridgell           */
/*---------------------------------------------------------------------------*/

gchar* s_strcpy (gchar *dest, const gchar *src, guint maxlength)
{
guint len;

	if (!dest) {
		printf ("ERROR: NULL dest in safe_strcpy\n");
		return NULL;
	}

	if (!src) {
		*dest = 0;
		return dest;
	}

	len = strlen(src);

	if (len > maxlength) {
		printf ("ERROR: string overflow by %d in safe_strcpy [%.50s]\n",
				(int)(len-maxlength), src);
		len = maxlength;
	}

	memcpy(dest, src, len);
	dest[len] = 0;
	return dest;
}

/*---------------------------------------------------------------------------*/

gchar* s_strcat (gchar *dest, const gchar *src, guint maxlength)
{
guint src_len, dest_len;

	if (!dest) {
		printf ("ERROR: NULL dest in safe_strcat\n");
		return NULL;
	}

	if (!src) {
		return dest;
	}

	src_len = strlen(src);
	dest_len = strlen(dest);

	if (src_len + dest_len > maxlength) {
		printf ("ERROR: string overflow by %d in safe_strcat [%.50s]\n",
				(int)(src_len + dest_len - maxlength), src);
		src_len = maxlength - dest_len;
	}

	memcpy(&dest[dest_len], src, src_len);
	dest[dest_len + src_len] = 0;
	return dest;
}

/*---------------------------------------------------------------------------*/

void read_config (void)
{

	if (s_prefs_openfile (s_getfilename_config (), P_READ)) {

		config.window_x = s_prefs_get_int ("window_pos_x");
		config.window_y = s_prefs_get_int ("window_pos_y");
		config.window_size_x = s_prefs_get_int ("window_size_x");
		config.window_size_y = s_prefs_get_int ("window_size_y");

		config.run_counter = s_prefs_get_int ("run_counter");

		config.pane_pos = s_prefs_get_int ("pane_pos");
		config.find_type = s_prefs_get_int ("find_type");
		config.find_activate = s_prefs_get_int ("find_activate");

		config.contact_window_x = s_prefs_get_int ("contact_window_x");
		config.contact_window_y = s_prefs_get_int ("contact_window_y");

		config.edit_window_x = s_prefs_get_int ("edit_window_x");
		config.edit_window_y = s_prefs_get_int ("edit_window_y");

		config.prefs_window_x = s_prefs_get_int ("prefs_window_x");
		config.prefs_window_y = s_prefs_get_int ("prefs_window_y");

		config.select_window_x = s_prefs_get_int ("select_window_x");
		config.select_window_y = s_prefs_get_int ("select_window_y");

		config.sorting_mode = s_prefs_get_int ("sorting_mode");

		config.toolbar_style = s_prefs_get_int ("toolbar_style");

		config.restore_windows_pos = s_prefs_get_int ("restore_windows_pos");

		config.selected_fields = s_prefs_get_int ("selected_fields");

		strncpy(config.web_browser, s_prefs_get_string ("web_browser"), MAX_COMMAND);
		strncpy(config.email_client, s_prefs_get_string ("email_client"), MAX_COMMAND);

		strncpy(config.font_header, s_prefs_get_string("font_header"), MAX_FONT_STR);
		strncpy(config.font_text, s_prefs_get_string("font_text"), MAX_FONT_STR);

		if(!strlen(config.font_header))
			strcpy(config.font_header, "Sans 11");
		if(!strlen(config.font_text))
			strcpy(config.font_text, "Serif 10");

		strncpy(config.color1, s_prefs_get_string ("color1"), MAX_COLOR_STR);
		strncpy(config.color2, s_prefs_get_string ("color2"), MAX_COLOR_STR);
		strncpy(config.color3, s_prefs_get_string ("color3"), MAX_COLOR_STR);
		strncpy(config.color4, s_prefs_get_string ("color4"), MAX_COLOR_STR);
		strncpy(config.color5, s_prefs_get_string ("color5"), MAX_COLOR_STR);

		s_prefs_closefile ();

	} else {

		config.window_x = 180;
		config.window_y = 100;
		config.window_size_x = 385;
		config.window_size_y = 500;

		config.run_counter = 0;

		config.pane_pos = 128;
		config.find_type = 0;
		config.find_activate = 0;

		config.contact_window_x = 20;
		config.contact_window_y = 20;

		config.edit_window_x = 40;
		config.edit_window_y = 30;

		config.prefs_window_x = 60;
		config.prefs_window_y = 60;

		config.select_window_x = 50;
		config.select_window_y = 50;

		config.sorting_mode = 1;

		config.toolbar_style = GTK_TOOLBAR_BOTH;

		config.restore_windows_pos = 1;

		config.selected_fields = -1;

		strcpy(config.web_browser, "mozilla -remote \"openurl(%s)\"");
		strcpy(config.email_client, "sylpheed --compose %s");

		strcpy(config.font_header, "Sans 11");
		strcpy(config.font_text, "Serif 10");

		strcpy(config.color1, "#016D5A");
		strcpy(config.color2, "#04344D");
		strcpy(config.color3, "#278B2B");
		strcpy(config.color4, "#1B2C8F");
		strcpy(config.color5, "#E8E8DE");

		write_config ();
	}

}

/*------------------------------------------------------------------------*/

void write_config(void)
{

	if (s_prefs_openfile (s_getfilename_config (), P_WRITE)) {

		s_prefs_put_int ("window_pos_x", config.window_x);
		s_prefs_put_int ("window_pos_y", config.window_y);
		s_prefs_put_int ("window_size_x", config.window_size_x);
		s_prefs_put_int ("window_size_y", config.window_size_y);

		s_prefs_put_int ("run_counter", config.run_counter);

		s_prefs_put_int ("pane_pos", config.pane_pos);
		s_prefs_put_int ("find_type", config.find_type);
		s_prefs_put_int ("find_activate", config.find_activate);

		s_prefs_put_int ("contact_window_x", config.contact_window_x);
		s_prefs_put_int ("contact_window_y", config.contact_window_y);

		s_prefs_put_int ("edit_window_x", config.edit_window_x);
		s_prefs_put_int ("edit_window_y", config.edit_window_y);

		s_prefs_put_int ("prefs_window_x", config.prefs_window_x);
		s_prefs_put_int ("prefs_window_y", config.prefs_window_y);

		s_prefs_put_int ("select_window_x", config.select_window_x);
		s_prefs_put_int ("select_window_y", config.select_window_y);

		s_prefs_put_int ("sorting_mode", config.sorting_mode);

		s_prefs_put_int ("toolbar_style", config.toolbar_style);

		s_prefs_put_int ("restore_windows_pos", config.restore_windows_pos);

		s_prefs_put_int ("selected_fields", config.selected_fields);

		s_prefs_put_string ("web_browser", config.web_browser);
		s_prefs_put_string ("email_client", config.email_client);

		s_prefs_put_string ("font_header", config.font_header);
		s_prefs_put_string ("font_text", config.font_text);

		s_prefs_put_string ("color1", config.color1);
		s_prefs_put_string ("color2", config.color2);
		s_prefs_put_string ("color3", config.color3);
		s_prefs_put_string ("color4", config.color4);
		s_prefs_put_string ("color5", config.color5);

	}

	s_prefs_closefile ();
}

/*------------------------------------------------------------------------*/

