
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gdk/gdk.h>
#include <sys/types.h>
#include <unistd.h>

#include "i18n.h"
#include "gui.h"
#include "data.h"
#include "prefs.h"
#include "actions.h"

static gchar command[MAX_PATH];

/*----------------------------------------------------------------------*/

void execute_command(gchar *command)
{
gchar *cmdline[4];
pid_t pid;

	pid = fork();

	if (pid == 0) {

		cmdline[0] = "sh";
		cmdline[1] = "-c";
		cmdline[2] = command;
		cmdline[3] = 0;
		execvp("/bin/sh", cmdline);
		_exit(1);

	}
}

/*----------------------------------------------------------------------*/

gint action_send_email(gchar *email)
{
	sprintf(command, config.email_client, email);
	execute_command(command);

	return 0;
}

/*----------------------------------------------------------------------*/

gint action_launch_website(gchar *url)
{
	sprintf(command, config.web_browser, url);
	execute_command(command);

	return 0;
}

/*----------------------------------------------------------------------*/

