
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <glib.h>
#include <string.h>

#include "gui.h"
#include "data.h"
#include "prefs.h"


int main(int argc, char **argv)
{
	read_config ();

	data_init ();
	data_read ();

#if ENABLE_NLS
		gtk_set_locale();
		bindtextdomain(PACKAGE, LOCALEDIR);
		textdomain(PACKAGE);
#endif

	config.run_counter++;

	gtk_init (&argc, &argv);
	gui_create_window ();

	gtk_main ();

	data_write();
	data_close();

	write_config ();

	return 0;
}

