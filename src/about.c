
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gui.h"
#include "data.h"
#include "about.h"
#include "i18n.h"
#include "prefs.h"
#include "logo.h"
#include "license.h"


static GtkWidget        *about_win;
static GtkWidget        *license_win;

/*----------------------------------------------------------------------------*/

void a_close_window( GtkWidget *widget, gpointer data )
{
	gtk_widget_destroy (about_win);
	gui_show_status(_("Idle"));
}

/*----------------------------------------------------------------------------*/

void gui_create_about_window(void)
{
GtkWidget       *vbox1;
GtkWidget       *vbox2;
GtkWidget       *hbox2;
GtkWidget       *label;
GtkWidget       *frame;
GtkWidget       *close_button;
GtkWidget       *hseparator;
GtkWidget       *hbutonbox;
GdkPixmap       *pixmap_m;
GtkWidget       *logo;
GdkBitmap       *mask;
GdkPixmap       *icon;
GdkBitmap       *icon_mask;
GtkStyle        *style;

	gui_show_status(_("Just send us a nice e-mail!"));

	about_win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for(GTK_WINDOW(about_win),GTK_WINDOW(main_window));

	gtk_window_set_modal(GTK_WINDOW(about_win), TRUE);
	gtk_window_set_resizable (GTK_WINDOW (about_win), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (about_win), 6);
	gtk_window_set_title (GTK_WINDOW (about_win), _("About"));
	gtk_widget_set_size_request (GTK_WIDGET(about_win), ab_width, ab_height);

	gdk_window_get_root_origin (main_window->window,
							&config.window_x, &config.window_y);

	gtk_window_move (GTK_WINDOW (about_win),
						((config.window_size_x - ab_width) >> 1) + config.window_x, 
						((config.window_size_y - ab_height) >> 1) + config.window_y);

	g_signal_connect (G_OBJECT (about_win), "delete_event",
						G_CALLBACK(a_close_window), NULL);

	gtk_widget_show(about_win);

	style = gtk_widget_get_style(GTK_WIDGET(about_win));
	icon = gdk_pixmap_create_from_xpm_d (about_win->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
	gdk_window_set_icon (about_win->window, NULL, icon, icon_mask);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_container_add (GTK_CONTAINER (about_win), frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (frame), vbox1);

	pixmap_m = gdk_pixmap_create_from_xpm_d(about_win->window, &mask, NULL, logo_xpm);

	logo = gtk_image_new_from_pixmap (pixmap_m, mask);
	gtk_widget_show (logo);
	gtk_box_pack_start (GTK_BOX (vbox1), logo, FALSE, TRUE, 0);

	label = gtk_label_new ("version "VERSION);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (vbox1), label, TRUE, FALSE, 8);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);

	label = gtk_label_new ("");
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, FALSE, 8);
	gtk_label_set_markup(GTK_LABEL(label), _("<b>CODE</b>"));

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);

	label = gtk_label_new ("Tomasz Mąka <pasp@ll.pl>");
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, FALSE, 0);

	/*----------------------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 6);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);

	label = gtk_label_new ("");
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, FALSE, 8);
	gtk_label_set_markup(GTK_LABEL(label), _("<b>GRAPHICS</b>"));

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);

	label = gtk_label_new ("Piotr Mąka <sill@clay.ll.pl>");
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, FALSE, 0);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, TRUE, TRUE, 0);

	label = gtk_label_new ("Maja Kocoń <ironya@ll.pl>");
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox2), label, TRUE, FALSE, 0);

	/*----------------------------------------------------------------------------*/

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	hbutonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbutonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbutonbox, FALSE, TRUE, 8);

	close_button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
	gtk_widget_show (close_button);
	g_signal_connect (G_OBJECT (close_button), "clicked",
						G_CALLBACK (a_close_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbutonbox), close_button);
	GTK_WIDGET_SET_FLAGS (close_button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (close_button);

}

/*----------------------------------------------------------------------------*/

void l_close_window( GtkWidget *widget, gpointer data )
{
	gtk_widget_destroy (license_win);
	gui_show_status(_("Idle"));
}

/*----------------------------------------------------------------------------*/

void gui_create_license_window(void)
{
GtkWidget	*vbox1;
GtkWidget	*frame;
GtkWidget	*close_button;
GtkWidget	*hbutonbox;
GtkWidget	*scrolled_window;
GtkTextBuffer	*entry_buffer = NULL;
GtkWidget	*text_sheet = NULL;
GtkTextIter 	iter;
GdkPixmap	*icon;
GdkBitmap	*icon_mask;
GtkStyle	*style;


	gui_show_status(_("Read carefully..."));

	license_win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for(GTK_WINDOW(license_win),GTK_WINDOW(main_window));

	gtk_window_set_modal(GTK_WINDOW(license_win), TRUE);
	gtk_window_set_resizable (GTK_WINDOW (license_win), TRUE);
	gtk_container_set_border_width (GTK_CONTAINER (license_win), 6);
	gtk_window_set_title (GTK_WINDOW (license_win), _("License"));
	gtk_widget_set_size_request (GTK_WIDGET(license_win), l_width, l_height);
	gdk_window_get_root_origin (main_window->window,
								&config.window_x, &config.window_y);

	gtk_window_move (GTK_WINDOW (license_win),
							config.window_x + 50, config.window_y + 35);

	g_signal_connect (G_OBJECT (license_win), "delete_event",
						G_CALLBACK(l_close_window), NULL);

	gtk_widget_show(license_win);

	style = gtk_widget_get_style(GTK_WIDGET(license_win));
	icon = gdk_pixmap_create_from_xpm_d (license_win->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
	gdk_window_set_icon (license_win->window, NULL, icon, icon_mask);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (license_win), vbox1);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (frame), scrolled_window);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolled_window);

	entry_buffer = gtk_text_buffer_new (NULL);
	gtk_text_buffer_get_iter_at_offset (entry_buffer, &iter, 0);

	text_sheet = gtk_text_view_new_with_buffer (entry_buffer);
	gtk_container_set_border_width (GTK_CONTAINER (text_sheet), 1);
	gtk_text_view_set_editable(GTK_TEXT_VIEW (text_sheet), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW (text_sheet), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_sheet), GTK_WRAP_WORD);
	gtk_widget_show (text_sheet);
	gtk_container_add (GTK_CONTAINER (scrolled_window), text_sheet);

	gtk_text_buffer_insert (entry_buffer, &iter, text_license, -1);

	hbutonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbutonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbutonbox, FALSE, TRUE, 4);

	close_button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
	gtk_widget_show (close_button);
	g_signal_connect (G_OBJECT (close_button), "clicked",
						G_CALLBACK (l_close_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbutonbox), close_button);
	GTK_WIDGET_SET_FLAGS (close_button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (close_button);

}

/*----------------------------------------------------------------------------*/


