
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <gtk/gtk.h>

#define config_filename 	"dlume"
#define config_a_filename 	"dlume-accels"

#define P_READ				1
#define P_WRITE				2

#define null_char			'\0'
#define crlf_char			'\n'
#define space_char			' '
#define comma_char			','
#define under_char			'_'
#define slash				"/"
#define space				" "
#define crlf				"\n"
#define dot					"."

#define MAX_LINE_LEN		4096
#define MAX_VALUE_LEN		2048

#define MAX_PATH			1024
#define MAX_FONT_STR		256
#define MAX_COLOR_STR		32
#define MAX_COMMAND			128

gchar*	s_strcpy	(gchar *dest, const gchar *src, guint maxlength);
gchar*	s_strcat	(gchar *dest, const gchar *src, guint maxlength);


struct dlumeprefs
{
	gint		window_x;
	gint		window_y;
	gint		window_size_x;
	gint		window_size_y;
	gint		run_counter;
	gint		pane_pos;
	gint		find_type;
	gint		find_activate;
	gint		contact_window_x;
	gint		contact_window_y;
	gint		edit_window_x;
	gint		edit_window_y;
	gint		prefs_window_x;
	gint		prefs_window_y;
	gint		select_window_x;
	gint		select_window_y;
	gint		sorting_mode;
	gint		toolbar_style;
	gint		restore_windows_pos;
	gint		selected_fields;
	gchar		web_browser[MAX_COMMAND];
	gchar		email_client[MAX_COMMAND];
	gchar		font_header[MAX_FONT_STR];
	gchar		font_text[MAX_FONT_STR];
	gchar		color1[MAX_COLOR_STR];
	gchar		color2[MAX_COLOR_STR];
	gchar		color3[MAX_COLOR_STR];
	gchar		color4[MAX_COLOR_STR];
	gchar		color5[MAX_COLOR_STR];
};

extern	struct dlumeprefs		config;

gchar*	s_getfilename_config	(void);
gchar*	s_getfilename_config_a	(void);
void	read_config				(void);
void	write_config			(void);


