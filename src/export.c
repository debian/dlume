
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gui.h"
#include "i18n.h"
#include "data.h"
#include "prefs.h"
#include "export.h"


static GtkWidget 	*file_requester;
static GtkWidget 	*select_window;
static GtkWidget	*check_buttons[nfields-1];

/*------------------------------------------------------------------------------*/

void gui_close_cancel_select_window( GtkWidget *widget, gpointer data )
{
	gdk_window_get_root_origin (select_window->window,
							&config.select_window_x, &config.select_window_y);

	gtk_widget_destroy (select_window);
	gui_show_status(_("Idle"));
}


void gui_close_select_window( GtkWidget *widget, gpointer data )
{
gint i;

	config.selected_fields = 0;

	for(i=0; i < nfields-1; i++) {
		if((check_buttons[i] != NULL) && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_buttons[i])) == TRUE)
			config.selected_fields |= (1 << i);
	}

	gdk_window_get_root_origin (select_window->window,
								&config.select_window_x, &config.select_window_y);

	gtk_widget_destroy (select_window);
	gui_show_status(_("Idle"));
}

/*------------------------------------------------------------------------------*/

void export_select_fields(void)
{
GtkWidget *vbox1;
GtkWidget *vbox2;
GtkWidget *vbox3;
GtkWidget *scrolledwindow;
GtkWidget *viewport;
GtkWidget *hbuttonbox;
GtkWidget *close_button;
GtkWidget *cancel_button;
GtkWidget *hseparator;
GtkWidget *frame;
gint i, j;

	gui_show_status(_("Select what you want..."));

	select_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for(GTK_WINDOW(select_window),GTK_WINDOW(main_window));
	gtk_window_set_title (GTK_WINDOW (select_window), _("Select fields to export..."));
	gtk_container_set_border_width (GTK_CONTAINER (select_window), 6);
	gtk_window_set_modal(GTK_WINDOW(select_window), TRUE);

	gtk_window_set_resizable (GTK_WINDOW (main_window), TRUE);
	gtk_widget_set_size_request (GTK_WIDGET(select_window), fs_width, fs_height);

	if(config.restore_windows_pos)
		gtk_window_move (GTK_WINDOW (select_window),
							config.select_window_x, config.select_window_y);

	g_signal_connect (G_OBJECT (select_window), "delete_event",
						G_CALLBACK(gui_close_cancel_select_window), NULL);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (select_window), vbox1);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
/*	gtk_container_add (GTK_CONTAINER (frame), vbox2);*/
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow);
	gtk_box_pack_start (GTK_BOX (vbox2), scrolledwindow, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	viewport = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport);
	gtk_container_add (GTK_CONTAINER (scrolledwindow), viewport);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_container_add (GTK_CONTAINER (viewport), frame);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);
/*		gtk_box_pack_start (GTK_BOX (vbox1), frame, TRUE, TRUE, 0);*/
	gtk_container_set_border_width (GTK_CONTAINER (frame), 4);


	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame), vbox3);
/*	gtk_container_add (GTK_CONTAINER (scrolledwindow), vbox3);*/

	/*----------------------------------------------------------*/

	check_buttons[FIRST_NAME] = gtk_check_button_new_with_mnemonic (_("First Name"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[FIRST_NAME]), 2);
	gtk_widget_show (check_buttons[FIRST_NAME]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[FIRST_NAME], TRUE, TRUE, 0);

	check_buttons[LAST_NAME] = gtk_check_button_new_with_mnemonic (_("Last Name"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[LAST_NAME]), 2);
	gtk_widget_show (check_buttons[LAST_NAME]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[LAST_NAME], TRUE, TRUE, 0);

	check_buttons[NICKNAME] = gtk_check_button_new_with_mnemonic (_("Nickname"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[NICKNAME]), 2);
	gtk_widget_show (check_buttons[NICKNAME]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[NICKNAME], TRUE, TRUE, 0);

	check_buttons[BIRTHDAY] = gtk_check_button_new_with_mnemonic (_("Birthday"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[BIRTHDAY]), 2);
	gtk_widget_show (check_buttons[BIRTHDAY]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[BIRTHDAY], TRUE, TRUE, 0);

	check_buttons[ADDRESS] = gtk_check_button_new_with_mnemonic (_("Address"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[ADDRESS]), 2);
	gtk_widget_show (check_buttons[ADDRESS]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[ADDRESS], TRUE, TRUE, 0);

	check_buttons[POST_CODE] = gtk_check_button_new_with_mnemonic (_("Post Code"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[POST_CODE]), 2);
	gtk_widget_show (check_buttons[POST_CODE]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[POST_CODE], TRUE, TRUE, 0);

	check_buttons[CITY] = gtk_check_button_new_with_mnemonic (_("City"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[CITY]), 2);
	gtk_widget_show (check_buttons[CITY]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[CITY], TRUE, TRUE, 0);

	check_buttons[STATE] = gtk_check_button_new_with_mnemonic (_("State"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[STATE]), 2);
	gtk_widget_show (check_buttons[STATE]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[STATE], TRUE, TRUE, 0);

	check_buttons[COUNTRY] = gtk_check_button_new_with_mnemonic (_("Country"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[COUNTRY]), 2);
	gtk_widget_show (check_buttons[COUNTRY]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[COUNTRY], TRUE, TRUE, 0);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	check_buttons[HOME_PHONE_1] = gtk_check_button_new_with_mnemonic (_("Home phone 1"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[HOME_PHONE_1]), 2);
	gtk_widget_show (check_buttons[HOME_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[HOME_PHONE_1], TRUE, TRUE, 0);

	check_buttons[HOME_PHONE_2] = gtk_check_button_new_with_mnemonic (_("Home phone 2"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[HOME_PHONE_2]), 2);
	gtk_widget_show (check_buttons[HOME_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[HOME_PHONE_2], TRUE, TRUE, 0);

	check_buttons[HOME_PHONE_3] = gtk_check_button_new_with_mnemonic (_("Home phone 3"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[HOME_PHONE_3]), 2);
	gtk_widget_show (check_buttons[HOME_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[HOME_PHONE_3], TRUE, TRUE, 0);

	check_buttons[HOME_PHONE_4] = gtk_check_button_new_with_mnemonic (_("Home phone 4"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[HOME_PHONE_4]), 2);
	gtk_widget_show (check_buttons[HOME_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[HOME_PHONE_4], TRUE, TRUE, 0);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	check_buttons[WORK_PHONE_1] = gtk_check_button_new_with_mnemonic (_("Work phone 1"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[WORK_PHONE_1]), 2);
	gtk_widget_show (check_buttons[WORK_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[WORK_PHONE_1], TRUE, TRUE, 0);

	check_buttons[WORK_PHONE_2] = gtk_check_button_new_with_mnemonic (_("Work phone 2"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[WORK_PHONE_2]), 2);
	gtk_widget_show (check_buttons[WORK_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[WORK_PHONE_2], TRUE, TRUE, 0);

	check_buttons[WORK_PHONE_3] = gtk_check_button_new_with_mnemonic (_("Work phone 3"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[WORK_PHONE_3]), 2);
	gtk_widget_show (check_buttons[WORK_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[WORK_PHONE_3], TRUE, TRUE, 0);

	check_buttons[WORK_PHONE_4] = gtk_check_button_new_with_mnemonic (_("Work phone 4"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[WORK_PHONE_4]), 2);
	gtk_widget_show (check_buttons[WORK_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[WORK_PHONE_4], TRUE, TRUE, 0);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	check_buttons[CELL_PHONE_1] = gtk_check_button_new_with_mnemonic (_("Cell phone 1"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[CELL_PHONE_1]), 2);
	gtk_widget_show (check_buttons[CELL_PHONE_1]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[CELL_PHONE_1], TRUE, TRUE, 0);

	check_buttons[CELL_PHONE_2] = gtk_check_button_new_with_mnemonic (_("Cell phone 2"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[CELL_PHONE_2]), 2);
	gtk_widget_show (check_buttons[CELL_PHONE_2]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[CELL_PHONE_2], TRUE, TRUE, 0);

	check_buttons[CELL_PHONE_3] = gtk_check_button_new_with_mnemonic (_("Cell phone 3"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[CELL_PHONE_3]), 2);
	gtk_widget_show (check_buttons[CELL_PHONE_3]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[CELL_PHONE_3], TRUE, TRUE, 0);

	check_buttons[CELL_PHONE_4] = gtk_check_button_new_with_mnemonic (_("Cell phone 4"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[CELL_PHONE_4]), 2);
	gtk_widget_show (check_buttons[CELL_PHONE_4]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[CELL_PHONE_4], TRUE, TRUE, 0);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	check_buttons[EMAIL_1] = gtk_check_button_new_with_mnemonic (_("E-mail 1"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[EMAIL_1]), 2);
	gtk_widget_show (check_buttons[EMAIL_1]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[EMAIL_1], TRUE, TRUE, 0);

	check_buttons[EMAIL_2] = gtk_check_button_new_with_mnemonic (_("E-mail 2"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[EMAIL_2]), 2);
	gtk_widget_show (check_buttons[EMAIL_2]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[EMAIL_2], TRUE, TRUE, 0);

	check_buttons[EMAIL_3] = gtk_check_button_new_with_mnemonic (_("E-mail 3"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[EMAIL_3]), 2);
	gtk_widget_show (check_buttons[EMAIL_3]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[EMAIL_3], TRUE, TRUE, 0);

	check_buttons[EMAIL_4] = gtk_check_button_new_with_mnemonic (_("E-mail 4"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[EMAIL_4]), 2);
	gtk_widget_show (check_buttons[EMAIL_4]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[EMAIL_4], TRUE, TRUE, 0);

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

	check_buttons[URL_1] = gtk_check_button_new_with_mnemonic (_("Website 1"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[URL_1]), 2);
	gtk_widget_show (check_buttons[URL_1]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[URL_1], TRUE, TRUE, 0);

	check_buttons[URL_2] = gtk_check_button_new_with_mnemonic (_("Website 2"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[URL_2]), 2);
	gtk_widget_show (check_buttons[URL_2]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[URL_2], TRUE, TRUE, 0);

	check_buttons[URL_3] = gtk_check_button_new_with_mnemonic (_("Website 3"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[URL_3]), 2);
	gtk_widget_show (check_buttons[URL_3]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[URL_3], TRUE, TRUE, 0);

	check_buttons[URL_4] = gtk_check_button_new_with_mnemonic (_("Website 4"));
	gtk_container_set_border_width (GTK_CONTAINER (check_buttons[URL_4]), 2);
	gtk_widget_show (check_buttons[URL_4]);
	gtk_box_pack_start (GTK_BOX (vbox3), check_buttons[URL_4], TRUE, TRUE, 0);


	/*----------------------------------------------------------*/

	hseparator = gtk_hseparator_new();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

	hbuttonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbuttonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox, FALSE, TRUE, 3);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (hbuttonbox), 16);

	cancel_button = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_show (cancel_button);
	g_signal_connect (G_OBJECT (cancel_button), "clicked",
							G_CALLBACK (gui_close_cancel_select_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), cancel_button);
	GTK_WIDGET_SET_FLAGS (cancel_button, GTK_CAN_DEFAULT);

	close_button = gtk_button_new_from_stock ("gtk-ok");
	gtk_widget_show (close_button);
	g_signal_connect (G_OBJECT (close_button), "clicked",
							G_CALLBACK (gui_close_select_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), close_button);
	GTK_WIDGET_SET_FLAGS (close_button, GTK_CAN_DEFAULT);

	gtk_widget_grab_focus (cancel_button);

	gtk_widget_show (select_window);

	for(i=0; i < nfields-1; i++) {
		j = (config.selected_fields >> i) & 1;
		if(j)
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_buttons[i]), TRUE);
	}
}

/*------------------------------------------------------------------------------*/

void req_csv_file_ok(GtkWidget *w, GtkFileSelection *fs)
{
	export_data_as_csv((gchar *)gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));
	gtk_widget_destroy(file_requester);
}

void req_html_file_ok(GtkWidget *w, GtkFileSelection *fs)
{
	export_data_as_html((gchar *)gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));
	gtk_widget_destroy(file_requester);
}

void req_file_cl(GtkWidget *w, gpointer *data)
{
	gtk_widget_destroy(file_requester);
}

/*------------------------------------------------------------------------------*/

void export_csv_select_file(void)
{

	if(!config.selected_fields)

		gui_show_status(_("Please select fields to export first."));

	else if(data_get_records()) {

		file_requester = gtk_file_selection_new(_("Export contacts as CSV file..."));
		gtk_window_set_modal(GTK_WINDOW(file_requester), TRUE);
		gtk_window_set_transient_for(GTK_WINDOW(file_requester),GTK_WINDOW(main_window));

		g_signal_connect(G_OBJECT(GTK_FILE_SELECTION(file_requester)->ok_button), 
							"clicked", G_CALLBACK (req_csv_file_ok), file_requester);
		g_signal_connect(G_OBJECT(GTK_FILE_SELECTION(file_requester)->cancel_button), 
						"clicked", G_CALLBACK (req_file_cl), file_requester);

		gtk_file_selection_set_filename (GTK_FILE_SELECTION(file_requester), "contacts.csv");
		gtk_widget_show(file_requester);

	} else {

		gui_show_status(_("No contacts to export."));

	}

}


void export_html_select_file(void)
{

	if(!config.selected_fields)

		gui_show_status(_("Please select fields to export first."));

	else if(data_get_records()) {

		file_requester = gtk_file_selection_new(_("Export contacts as HTML file..."));
		gtk_window_set_modal(GTK_WINDOW(file_requester), TRUE);
		gtk_window_set_transient_for(GTK_WINDOW(file_requester),GTK_WINDOW(main_window));

		g_signal_connect(G_OBJECT(GTK_FILE_SELECTION(file_requester)->ok_button), 
							"clicked", G_CALLBACK (req_html_file_ok), file_requester);
		g_signal_connect(G_OBJECT(GTK_FILE_SELECTION(file_requester)->cancel_button), 
						"clicked", G_CALLBACK (req_file_cl), file_requester);

		gtk_file_selection_set_filename (GTK_FILE_SELECTION(file_requester), "contacts.html");
		gtk_widget_show(file_requester);

	} else {

		gui_show_status(_("No contacts to export."));

	}

}

/*------------------------------------------------------------------------------*/

gint export_data_as_csv(gchar *filename)
{
FILE *filehandle;
gint i, j, n;
recordstr *rdata;

	filehandle = fopen (filename, "w");

	if(filehandle) {

		/* count rows */
		j = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);

		for(i=0; i < j; i++) {

			n = find_record(gui_get_column_content(i, 0), gui_get_column_content(i, 1));

			if (n!=-1) {
				rdata = g_slist_nth_data (list, n);

				if (rdata)
					data_print_csv_entry(filehandle, rdata);
			}
		}

		fclose(filehandle);
	}

	return 0;
}

/*------------------------------------------------------------------------------*/

gint export_data_as_html(gchar *filename)
{
FILE *filehandle;
gint i, j, n, m, k, e;
recordstr *rdata;

gchar *html_head[] = {
	"FIRST&nbsp;NAME", "LAST&nbsp;NAME",
	"NICKNAME", "BIRTHDAY",
	"ADDRESS", "POST&nbsp;CODE",
	"CITY", "STATE",
	"COUNTRY",
	"HOME&nbsp;PHONE&nbsp;1", "HOME&nbsp;PHONE&nbsp;2",
	"HOME&nbsp;PHONE&nbsp;3", "HOME&nbsp;PHONE&nbsp;4",
	"WORK&nbsp;PHONE&nbsp;1", "WORK&nbsp;PHONE&nbsp;2",
	"WORK&nbsp;PHONE&nbsp;3", "WORK&nbsp;PHONE&nbsp;4",
	"CELL&nbsp;PHONE&nbsp;1", "CELL&nbsp;PHONE&nbsp;2",
	"CELL&nbsp;PHONE&nbsp;3", "CELL&nbsp;PHONE&nbsp;4",
	"E-MAIL&nbsp;1", "E-MAIL&nbsp;2",
	"E-MAIL&nbsp;3", "E-MAIL&nbsp;4",
	"WEBSITE&nbsp;1", "WEBSITE&nbsp;2",
	"WEBSITE&nbsp;3", "WEBSITE&nbsp;4"
};


	filehandle = fopen (filename, "w");

	if(filehandle) {
		fprintf(filehandle, "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
		fprintf(filehandle, "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
		fprintf(filehandle, "<head>\n");
		fprintf(filehandle, "\t<title>Contact List</title>\n");
		fprintf(filehandle, "\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
		fprintf(filehandle, "\t<meta name=\"generator\" content=\"dlume 0.2.2 - http://clay.ll.pl/dlume.html\" />\n");
		fprintf(filehandle, "\t<style type=\"text/css\">\n");
		fprintf(filehandle, "\t\tbody { color:black; background-color:white; }\n");
		fprintf(filehandle, "\t\ta { color:#0000ff; }\n");
		fprintf(filehandle, "\t\ta:visited { color:#000055; }\n");
		fprintf(filehandle, "\t\ttable { border-collapse:collapse; }\n");
		fprintf(filehandle, "\t\ttr.header { background-color:#c0c0c0; }\n");
		fprintf(filehandle, "\t\ttr.evenrow { background-color:#f0f0f0; }\n");
		fprintf(filehandle, "\t\ttr.oddrow { background-color:#fafafa; }\n");
		fprintf(filehandle, "\t\tth, td { border:1px solid #555555; padding:3px; }\n");
		fprintf(filehandle, "\t</style>\n");
		fprintf(filehandle, "</head>\n\n");
		fprintf(filehandle, "<body>\n\n");
		fprintf(filehandle, "<h1>Contact List</h1>\n\n");

		fprintf(filehandle, "<table>\n");

		fprintf(filehandle, "<tr class=\"header\">\n");

		for(k=0; k < nfields-1; k++) {

			e = (config.selected_fields >> k) & 1;

			if(e) {
				fprintf(filehandle, "\t<th>");
				fprintf(filehandle, html_head[k]);
				fprintf(filehandle, "</th>\n");
			}
		}

		fprintf(filehandle, "</tr>\n\n");

		/* count rows */
		j = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);

		for(i = m = 0; i < j; i++, m++) {

			n = find_record(gui_get_column_content(i, 0), gui_get_column_content(i, 1));

			if (n!=-1) {
				
				rdata = g_slist_nth_data (list, n);

				if (rdata) {

					if(m & 1)
						fprintf(filehandle, "<tr class=\"evenrow\">\n");
					else
						fprintf(filehandle, "<tr class=\"oddrow\">\n");

					data_print_html_entry(filehandle, rdata);
					fprintf(filehandle, "</tr>\n\n");
				}

			}
		}


		fprintf(filehandle, "</table>\n");

		fprintf(filehandle, "</body>\n");
		fprintf(filehandle, "</html>\n");

		fclose(filehandle);
	}

	return 0;
}

/*------------------------------------------------------------------------------*/

