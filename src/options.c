
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <string.h>

#include "gui.h"
#include "data.h"
#include "options.h"
#include "i18n.h"
#include "prefs.h"

/* prefs window */
static GtkWidget	*prefs_window;
static GtkWidget	*color1_button;
static GtkWidget	*color2_button;
static GtkWidget	*color3_button;
static GtkWidget	*color4_button;
static GtkWidget	*color5_button;
static GtkWidget	*wb_entry;
static GtkWidget	*em_entry;
static GtkWidget	*fn_radiobutton;
static GtkWidget	*tb_icons_radiobutton;
static GtkWidget	*tb_text_radiobutton;
static GtkWidget	*tb_both_radiobutton;
static GtkWidget	*aqs_check_button;
static GtkWidget	*sws_check_button;

/* color selector */
static GtkWidget	*cs;

static gint			v_restart;
static gchar		tmpbuf[TMPBUF_SIZE];

/* font selector */
static GtkWidget	*fontname_entry;

/*------------------------------------------------------------------------------*/

#define n_buttons	5

void gui_set_buttons_colors(void)
{
gint i;
GdkColor color;
GtkWidget *wdg[] = { color1_button, color2_button, color3_button, 
						color4_button, color5_button };
gchar *cstr[] = { config.color1, config.color2, config.color3,
					config.color4, config.color5 };

	for(i=0; i < n_buttons; i++) {

		gdk_color_parse (cstr[i], &color);
		gtk_widget_modify_bg(GTK_WIDGET(wdg[i]), GTK_STATE_NORMAL, &color);
		gtk_widget_modify_bg(GTK_WIDGET(wdg[i]), GTK_STATE_PRELIGHT, &color);
		gtk_widget_modify_bg(GTK_WIDGET(wdg[i]), GTK_STATE_ACTIVE, &color);

	}

}

/*------------------------------------------------------------------------------*/

static void quote_colors_set_dialog_ok(GtkWidget *widget, gpointer data)
{
GtkColorSelection *colorsel;
GdkColor color;

	colorsel = (GtkColorSelection *) ((GtkColorSelectionDialog *)cs)->colorsel;

	gtk_color_selection_get_current_color(colorsel, &color);

	switch ((gint)data) {

		case 0:
				strcpy(config.color1, gtk_color_selection_palette_to_string(&color, 1));
		        break;
		case 1:
				strcpy(config.color2, gtk_color_selection_palette_to_string(&color, 1));
				break;
		case 2:
				strcpy(config.color3, gtk_color_selection_palette_to_string(&color, 1));
				break;
		case 3:
				strcpy(config.color4, gtk_color_selection_palette_to_string(&color, 1));
				break;
		case 4:
				strcpy(config.color5, gtk_color_selection_palette_to_string(&color, 1));
				break;

		default:
				break;

	}

	gui_set_buttons_colors();
	gtk_widget_destroy(cs);

	v_restart = TRUE;
}

static void quote_colors_set_dialog_cancel(GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy(cs);
}

static void quote_colors_set_dialog_key_pressed(GtkWidget *widget,
						GdkEventKey *event,
						gpointer data)
{
	gtk_widget_destroy(cs);
}


void color_dialog(gint icolor)
{
GtkColorSelection *colorsel;
GdkColor ccolor;

	cs = gtk_color_selection_dialog_new (_("Select color"));
	gtk_window_set_modal(GTK_WINDOW(cs), TRUE);
	gtk_window_set_transient_for(GTK_WINDOW(cs), GTK_WINDOW(prefs_window));
	gtk_window_set_position(GTK_WINDOW(cs), GTK_WIN_POS_CENTER);
	gtk_window_set_resizable (GTK_WINDOW (cs), TRUE);

	colorsel = (GtkColorSelection *) ((GtkColorSelectionDialog *)cs)->colorsel;

	switch (icolor) {

		case 0:
			gdk_color_parse (config.color1, &ccolor);
			break;
		case 1:
			gdk_color_parse (config.color2, &ccolor);
			break;
		case 2:
			gdk_color_parse (config.color3, &ccolor);
			break;
		case 3:
			gdk_color_parse (config.color4, &ccolor);
			break;
		case 4:
			gdk_color_parse (config.color5, &ccolor);
			break;

		default:
			break;

	}

	gtk_color_selection_set_current_color(colorsel, &ccolor);

	g_signal_connect(G_OBJECT(GTK_COLOR_SELECTION_DIALOG(cs)->ok_button),
				"clicked", G_CALLBACK(quote_colors_set_dialog_ok), (gpointer *) icolor);
	g_signal_connect(G_OBJECT(GTK_COLOR_SELECTION_DIALOG(cs)->cancel_button),
				"clicked", G_CALLBACK(quote_colors_set_dialog_cancel), (gpointer *) 1);
	g_signal_connect(G_OBJECT(cs), "key_press_event",
				G_CALLBACK(quote_colors_set_dialog_key_pressed), (gpointer *) 0);

	gtk_widget_show(cs);
}

/*------------------------------------------------------------------------------*/

void cal_color_select( GtkWidget *widget, gpointer data )
{
	color_dialog((gint) data);
}

/*------------------------------------------------------------------------------*/
/* cancel callback */

void gui_cancel_prefs_window( GtkWidget *widget, gpointer data )
{
	gdk_window_get_root_origin (prefs_window->window,
						&config.prefs_window_x, &config.prefs_window_y);

	gtk_widget_destroy (prefs_window);
	gui_show_status(_("Idle"));
}

/*------------------------------------------------------------------------------*/

void gui_show_restart_info(void)
{
GtkWidget	*info_dialog;

	info_dialog = gtk_message_dialog_new (GTK_WINDOW (main_window),
					GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
					GTK_MESSAGE_INFO,
					GTK_BUTTONS_OK,
					_("You will need to restart dlume for these\nchanges to take effect."));

	gtk_widget_show (info_dialog);

	g_signal_connect (G_OBJECT(info_dialog), "response",
						G_CALLBACK (gtk_widget_destroy), NULL);

}

/*------------------------------------------------------------------------------*/
/* close window callback */

void gui_close_prefs_window( GtkWidget *widget, gpointer data )
{
gint ls;

	if(config.restore_windows_pos)
		gdk_window_get_root_origin (prefs_window->window,
							&config.prefs_window_x, &config.prefs_window_y);

	strcpy(config.web_browser, gtk_entry_get_text(GTK_ENTRY(wb_entry)));
	strcpy(config.email_client, gtk_entry_get_text(GTK_ENTRY(em_entry)));

	ls = config.sorting_mode;

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(fn_radiobutton)) == TRUE)
		config.sorting_mode = 0;
	else
		config.sorting_mode = 1;

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tb_icons_radiobutton)) == TRUE)
		config.toolbar_style = GTK_TOOLBAR_ICONS;
	else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tb_text_radiobutton)) == TRUE)
			config.toolbar_style = GTK_TOOLBAR_TEXT;
		else
			config.toolbar_style = GTK_TOOLBAR_BOTH;

	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), config.toolbar_style);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(aqs_check_button)))
		config.find_activate = 1;
	else
		config.find_activate = 0;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(sws_check_button)))
		config.restore_windows_pos = 1;
	else
		config.restore_windows_pos = 0;

	if(config.sorting_mode == ls) {

		if(config.sorting_mode) {
			g_signal_emit_by_name (column_ln, "clicked");
			g_signal_emit_by_name (column_ln, "clicked");
		} else {
			g_signal_emit_by_name (column_fn, "clicked");
			g_signal_emit_by_name (column_fn, "clicked");
		}

	} else {

		if(config.sorting_mode)
			g_signal_emit_by_name (column_ln, "clicked");
		else
			g_signal_emit_by_name (column_fn, "clicked");

	}

	gui_display_entry (current_fn, current_ln);
	gtk_widget_destroy (prefs_window);

	if(v_restart == TRUE) gui_show_restart_info();

	gui_show_status(_("Idle"));
}

/*------------------------------------------------------------------------------*/

void gui_font_select(GtkWidget *widget)
{
gchar *s;
GtkWidget *font_selector;
gint response;
PangoFontDescription *pfd;

	font_selector = gtk_font_selection_dialog_new ("Select a font...");
	gtk_window_set_modal(GTK_WINDOW(font_selector), TRUE);
	gtk_window_set_transient_for(GTK_WINDOW(font_selector), GTK_WINDOW(prefs_window));
	gtk_font_selection_dialog_set_font_name (GTK_FONT_SELECTION_DIALOG
						(font_selector),
						config.font_header);
	gtk_widget_show (font_selector);
	response = gtk_dialog_run (GTK_DIALOG (font_selector));
	if (response == GTK_RESPONSE_OK)
	{
		s = gtk_font_selection_dialog_get_font_name (GTK_FONT_SELECTION_DIALOG(font_selector));

		strncpy(config.font_header, s, MAX_FONT_STR);
		gtk_entry_set_text(GTK_ENTRY(fontname_entry), s);

		pfd = pango_font_description_from_string(gtk_font_selection_dialog_get_font_name
			(GTK_FONT_SELECTION_DIALOG (font_selector)));

		gtk_widget_modify_font (text_sheet, pfd);
		gui_display_entry (current_fn, current_ln);

		g_free (s);
		pango_font_description_free (pfd);
	}

	gtk_widget_destroy (font_selector);

}

/*------------------------------------------------------------------------------*/

void gui_create_prefs_window(void)
{
GtkWidget		*vbox1, *vbox2, *vbox3;
GtkWidget		*hbox1, *hbox2, *hbox3, *hbox4;
GSList			*fn_radiobutton_group = NULL;
GSList			*tb_radiobutton_group = NULL;
GtkWidget		*frame;
GtkWidget		*ln_radiobutton;
GtkWidget		*label;
GtkWidget		*hbuttonbox;
GtkWidget		*cancel_button;
GtkWidget		*close_button;
GdkPixmap		*icon;
GdkBitmap		*icon_mask;
GtkStyle		*style;
GtkWidget		*hseparator;
GtkSizeGroup	*sg_entry, *sg_label;
GtkTooltips		*color_tooltips;
GtkWidget		*fontsel_button;

	v_restart = FALSE;

	gui_show_status(_("Now it's time to customize the Dlume!"));

	color_tooltips = gtk_tooltips_new();

	sg_entry = gtk_size_group_new(GTK_SIZE_GROUP_BOTH);
	sg_label = gtk_size_group_new(GTK_SIZE_GROUP_BOTH);

	prefs_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_transient_for(GTK_WINDOW(prefs_window),GTK_WINDOW(main_window));
	gtk_container_set_border_width (GTK_CONTAINER (prefs_window), 6);
	gtk_window_set_title (GTK_WINDOW (prefs_window), _("Preferences"));

	gtk_window_set_modal(GTK_WINDOW(prefs_window), TRUE);

	gtk_window_set_resizable (GTK_WINDOW (prefs_window), TRUE);
	gtk_widget_set_size_request (GTK_WIDGET(prefs_window), pr_width, pr_height);

	if(config.restore_windows_pos)
		gtk_window_move (GTK_WINDOW (prefs_window),
						config.prefs_window_x, config.prefs_window_y);

	g_signal_connect (G_OBJECT (prefs_window), "delete_event",
						G_CALLBACK(gui_close_prefs_window), NULL);

	/*----------------------------------------------------------------------*/

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (prefs_window), vbox1);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox4 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox4);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox4, FALSE, TRUE, 0);

	fn_radiobutton = gtk_radio_button_new_with_mnemonic (NULL, _("First Name"));
	gtk_widget_show (fn_radiobutton);
	gtk_container_set_border_width (GTK_CONTAINER (fn_radiobutton), 4);
	gtk_box_pack_start (GTK_BOX (hbox4), fn_radiobutton, TRUE, TRUE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (fn_radiobutton), fn_radiobutton_group);
	fn_radiobutton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (fn_radiobutton));

	ln_radiobutton = gtk_radio_button_new_with_mnemonic (NULL, _("Last Name"));
	gtk_widget_show (ln_radiobutton);
	gtk_container_set_border_width (GTK_CONTAINER (ln_radiobutton), 4);
	gtk_box_pack_start (GTK_BOX (hbox4), ln_radiobutton, TRUE, TRUE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (ln_radiobutton), fn_radiobutton_group);
	fn_radiobutton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (ln_radiobutton));

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Sort by:</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------*/

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, FALSE, TRUE, 0);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, FALSE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	aqs_check_button = gtk_check_button_new_with_mnemonic ("Activate Quick-search");
	gtk_container_set_border_width (GTK_CONTAINER (aqs_check_button), 2);
	gtk_widget_show (aqs_check_button);
	gtk_box_pack_start (GTK_BOX (vbox2), aqs_check_button, FALSE, TRUE, 0);

	sws_check_button = gtk_check_button_new_with_mnemonic ("Restore windows positions");
	gtk_container_set_border_width (GTK_CONTAINER (sws_check_button), 2);
	gtk_widget_show (sws_check_button);
	gtk_box_pack_start (GTK_BOX (vbox2), sws_check_button, FALSE, TRUE, 0);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>On startup:</b></i> "));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------*/

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox1, TRUE, TRUE, 0);

	tb_icons_radiobutton = gtk_radio_button_new_with_mnemonic (NULL, _("Icons"));
	gtk_widget_show (tb_icons_radiobutton);
	gtk_container_set_border_width (GTK_CONTAINER (tb_icons_radiobutton), 4);
	gtk_box_pack_start (GTK_BOX (hbox1), tb_icons_radiobutton, TRUE, TRUE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (tb_icons_radiobutton), tb_radiobutton_group);
	tb_radiobutton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (tb_icons_radiobutton));

	tb_text_radiobutton = gtk_radio_button_new_with_mnemonic (NULL, _("Text"));
	gtk_widget_show (tb_text_radiobutton);
	gtk_container_set_border_width (GTK_CONTAINER (tb_text_radiobutton), 4);
	gtk_box_pack_start (GTK_BOX (hbox1), tb_text_radiobutton, TRUE, TRUE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (tb_text_radiobutton), tb_radiobutton_group);
	tb_radiobutton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (tb_text_radiobutton));

	tb_both_radiobutton = gtk_radio_button_new_with_mnemonic (NULL, _("Both"));
	gtk_widget_show (tb_both_radiobutton);
	gtk_container_set_border_width (GTK_CONTAINER (tb_both_radiobutton), 4);
	gtk_box_pack_start (GTK_BOX (hbox1), tb_both_radiobutton, TRUE, TRUE, 0);
	gtk_radio_button_set_group (GTK_RADIO_BUTTON (tb_both_radiobutton), tb_radiobutton_group);
	tb_radiobutton_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (tb_both_radiobutton));

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Display toolbar as:</b></i> "));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------*/

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, TRUE, TRUE, 0);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox1, TRUE, TRUE, 0);

	color1_button = gtk_button_new_with_mnemonic ("");
	gtk_widget_show (color1_button);
	gtk_box_pack_start (GTK_BOX (hbox1), color1_button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (color1_button), "clicked",
						G_CALLBACK (cal_color_select), (gpointer *) 0);
	gtk_container_set_border_width (GTK_CONTAINER (color1_button), 8);
	gtk_tooltips_set_tip (color_tooltips, color1_button, "header color", NULL);

	color2_button = gtk_button_new_with_mnemonic ("");
	gtk_widget_show (color2_button);
	gtk_box_pack_start (GTK_BOX (hbox1), color2_button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (color2_button), "clicked",
						G_CALLBACK (cal_color_select), (gpointer *) 1);
	gtk_container_set_border_width (GTK_CONTAINER (color2_button), 8);
	gtk_tooltips_set_tip (color_tooltips, color2_button, "data color", NULL);

	color3_button = gtk_button_new_with_mnemonic ("");
	gtk_widget_show (color3_button);
	gtk_box_pack_start (GTK_BOX (hbox1), color3_button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (color3_button), "clicked",
						G_CALLBACK (cal_color_select), (gpointer *) 2);
	gtk_container_set_border_width (GTK_CONTAINER (color3_button), 8);
	gtk_tooltips_set_tip (color_tooltips, color3_button, "e-mail color", NULL);

	color4_button = gtk_button_new_with_mnemonic ("");
	gtk_widget_show (color4_button);
	gtk_box_pack_start (GTK_BOX (hbox1), color4_button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (color4_button), "clicked",
						G_CALLBACK (cal_color_select), (gpointer *) 3);
	gtk_container_set_border_width (GTK_CONTAINER (color4_button), 8);
	gtk_tooltips_set_tip (color_tooltips, color4_button, "url color", NULL);

	color5_button = gtk_button_new_with_mnemonic ("");
	gtk_widget_show (color5_button);
	gtk_box_pack_start (GTK_BOX (hbox1), color5_button, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (color5_button), "clicked",
						G_CALLBACK (cal_color_select), (gpointer *) 4);
	gtk_container_set_border_width (GTK_CONTAINER (color5_button), 8);
	gtk_tooltips_set_tip (color_tooltips, color5_button, "background", NULL);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Colors:</b></i> "));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox1), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox1, FALSE, TRUE, 0);

	fontname_entry = gtk_entry_new ();
	gtk_widget_show (fontname_entry);
	gtk_box_pack_start (GTK_BOX (hbox1), fontname_entry, TRUE, TRUE, 2);
	gtk_editable_set_editable (GTK_EDITABLE (fontname_entry), FALSE);

	fontsel_button = gui_stock_button (GTK_STOCK_SELECT_FONT);
	gtk_widget_show (fontsel_button);
	g_signal_connect (G_OBJECT (fontsel_button), "clicked",
						G_CALLBACK (gui_font_select), NULL);
	gtk_box_pack_start (GTK_BOX (hbox1), fontsel_button, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (fontsel_button), 4);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Font:</b></i> "));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 4);

	/*----------------------------------------------------------------------*/

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame), vbox3);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox2, TRUE, TRUE, 0);

	wb_entry = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(wb_entry), TMPBUF_SIZE);
	gtk_widget_show (wb_entry);
	gtk_box_pack_start (GTK_BOX (hbox2), wb_entry, TRUE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, wb_entry);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>Web browser:</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

/*------------------------------------------------------------------------------*/

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (vbox2), frame, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_frame_set_shadow_type (GTK_FRAME(frame), GTK_SHADOW_NONE);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame), vbox3);

	hbox3 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox3);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox3, TRUE, TRUE, 0);

	em_entry = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(em_entry), TMPBUF_SIZE);
	gtk_widget_show (em_entry);
	gtk_box_pack_start (GTK_BOX (hbox3), em_entry, TRUE, TRUE, 2);
	gtk_size_group_add_widget(sg_entry, em_entry);

	label = gtk_label_new (NULL);
	gtk_widget_show (label);
	gtk_frame_set_label_widget (GTK_FRAME (frame), label);
	sprintf (tmpbuf, _("<i><b>E-mail client:</b></i>"));
	gtk_label_set_markup (GTK_LABEL (label), tmpbuf);

	/*----------------------------------------------------------------------*/

	hseparator = gtk_hseparator_new ();
	gtk_widget_show (hseparator);
	gtk_box_pack_start (GTK_BOX (vbox1), hseparator, FALSE, TRUE, 4);

	hbuttonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbuttonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox, FALSE, TRUE, 4);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (hbuttonbox), 16);

	cancel_button = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_show (cancel_button);
	g_signal_connect (G_OBJECT (cancel_button), "clicked",
						G_CALLBACK (gui_cancel_prefs_window), NULL);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), cancel_button);
	GTK_WIDGET_SET_FLAGS (cancel_button, GTK_CAN_DEFAULT);

	close_button = gtk_button_new_from_stock ("gtk-ok");
	gtk_widget_show (close_button);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), close_button);
	g_signal_connect (G_OBJECT (close_button), "clicked",
						G_CALLBACK (gui_close_prefs_window), NULL);
	GTK_WIDGET_SET_FLAGS (close_button, GTK_CAN_DEFAULT);

	gtk_widget_grab_focus (cancel_button);

	gui_set_buttons_colors();

	gtk_entry_set_text(GTK_ENTRY(wb_entry), config.web_browser);
	gtk_entry_set_text(GTK_ENTRY(em_entry), config.email_client);

	if(config.find_activate)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (aqs_check_button), TRUE);

	if(config.restore_windows_pos)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (sws_check_button), TRUE);

	if(!config.sorting_mode)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fn_radiobutton), TRUE);
	else
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ln_radiobutton), TRUE);

	if(config.toolbar_style == GTK_TOOLBAR_ICONS)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tb_icons_radiobutton), TRUE);
	if(config.toolbar_style == GTK_TOOLBAR_TEXT)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tb_text_radiobutton), TRUE);
	if(config.toolbar_style == GTK_TOOLBAR_BOTH)
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tb_both_radiobutton), TRUE);

	gtk_entry_set_text(GTK_ENTRY(fontname_entry), config.font_header);

	gtk_widget_show (prefs_window);

	style = gtk_widget_get_style(GTK_WIDGET(prefs_window));
	icon = gdk_pixmap_create_from_xpm_d (prefs_window->window, &icon_mask, &style->bg[GTK_STATE_NORMAL], icon_xpm);
	gdk_window_set_icon (prefs_window->window, NULL, icon, icon_mask);

}

/*------------------------------------------------------------------------------*/

