
/*
 * Dlume
 *
 * Copyright (C) 2003-2004 Tomasz M�ka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gui.h"
#include "data.h"
#include "delete.h"
#include "i18n.h"
#include "prefs.h"

/* delete record window */
static GtkWidget	*delete_window;


/*------------------------------------------------------------------------------*/
/* delete action callback */

void gui_action_delete_window( GtkWidget *widget, gpointer data )
{
gint k;
GtkTextIter a_iter, b_iter;

	gtk_widget_destroy (delete_window);

	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		data_remove_record(k);

		gui_show_status(_("Contact deleted."));

		gtk_text_buffer_get_bounds (entry_textinfo, &a_iter, &b_iter);
		gtk_text_buffer_delete (entry_textinfo, &a_iter, &b_iter);

		gui_display_records_list();
		gui_update_records_list((gchar *)gtk_entry_get_text(GTK_ENTRY(find_entry)));

	}

}

/*------------------------------------------------------------------------------*/
/* close window callback */

void gui_close_delete_window( GtkWidget *widget, gpointer data )
{
	gtk_widget_destroy (delete_window);
	gui_show_status(_("Idle"));
}

/*------------------------------------------------------------------------------*/

void gui_create_delete_window(void)
{
GtkTextIter		a_iter, b_iter;
GtkWidget		*info_dialog;
gint			k, response;
GtkWidget		*hbox;
GtkWidget		*label;
GtkWidget		*stock;


	k = find_record(current_fn, current_ln);

	if (k!=-1) {

		gui_show_status(_("There is no way to get the contact back..."));

		info_dialog = gtk_dialog_new_with_buttons ("Question",
						GTK_WINDOW (main_window),
						GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_STOCK_NO,
						GTK_RESPONSE_NO,
						GTK_STOCK_YES,
						GTK_RESPONSE_OK,
						NULL);

		hbox = gtk_hbox_new (FALSE, 8);
		gtk_container_set_border_width (GTK_CONTAINER (hbox), 8);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (info_dialog)->vbox), hbox, FALSE, FALSE, 0);

		stock = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
		gtk_box_pack_start (GTK_BOX (hbox), stock, FALSE, FALSE, 0);

		label = gtk_label_new (_("Delete current contact ?"));
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

		gtk_widget_show_all (hbox);

		response = gtk_dialog_run (GTK_DIALOG (info_dialog));

		if (response == GTK_RESPONSE_OK) {

			k = find_record(current_fn, current_ln);

			if (k!=-1) {

				data_remove_record(k);

				gui_show_status(_("Contact deleted."));

				gtk_text_buffer_get_bounds (entry_textinfo, &a_iter, &b_iter);
				gtk_text_buffer_delete (entry_textinfo, &a_iter, &b_iter);

				gui_display_records_list();
				gui_update_records_list((gchar *)gtk_entry_get_text(GTK_ENTRY(find_entry)));

			}

		}

		gtk_widget_destroy(info_dialog);
		gui_show_status(_("Idle"));

	} else {

		if(data_get_records())
			gui_show_status(_("Please select contact to delete first."));
		else
			gui_show_status(_("No contacts to delete."));

	}

}

/*------------------------------------------------------------------------------*/

